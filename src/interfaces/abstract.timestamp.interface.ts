import IAbstractInterface from './abstract.interface';

export default interface IAbstractTimeStampInterface extends IAbstractInterface {
    createdAt: Date;
    updatedAt: Date;
}
