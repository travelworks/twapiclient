import {IAttachment, IEmailTemplate} from './emailTemplate.interface';
import IAbstractInterface from '../abstract.interface';

export interface IEmail extends IAbstractInterface {
    emailTemplate: IEmailTemplate;
    placeholders: IEmail.IPlaceholders;
    from: string;
    to: string[];
    cc: string[];
    bcc: string[];
    subject: string;
    text: string;
    html: string;
    send_status: IEmail.ISendStatus;
    attachments: IAttachment[];
    createdAt: Date;

}

export namespace IEmail {

    export enum SendStatus {
        new = 'new',
        send = 'send',
        error = 'error'
    }

    export interface ISendStatus extends IAbstractInterface {
        status: SendStatus;
        timestamp: Date;
        error: string;
        result: any;
    }

    export interface IPlaceholders {
        [key: string]: any,
    }

}
