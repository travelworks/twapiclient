import IAbstractInterface from '../abstract.interface';
import {IBooking} from './booking.interface';
import {IPointAccount} from '../..';

export namespace ICustomer {
    export enum Gender {
        male = 'male',
        female = 'female',
        diverse = 'diverse'
    }


    export enum SubType {
        customer = 'customer',
        applicant = 'applicant',
        other = 'other'
    }

    export interface IAddress {
        street: string;
        zip: string;
        city: string;
        country: string;
        additional: string;
    }

    export interface IEmail {
        email: string;
        description: string;
    }

    export interface INote extends IAbstractInterface {
        customer: string;
        createdAt: Date;
        updatedAt: Date;
        type: INote.Type;
        contact_method: INote.ContactType;
        contact_person: string;
        content: string;
        probability?: INote.Probability;
    }

    export namespace INote {
        export enum Probability {
            likely = 'likely',
            maybe = 'maybe',
            unlikely = 'unlikely',
        }

        export enum Type {
            initial_inquiry = 'initial_inquiry',
            consultation = 'consultation',
            offer = 'offer',
            complaint = 'complaint'
        }

        export enum ContactType {
            email = 'email',
            phone = 'phone',
            web = 'web',
            letter = 'letter'
        }

    }

    export interface IAdditionalAttribute extends IAbstractInterface {
        value: string;
        code: string;
        title: string;
        valid_from: Date;
        valid_to: Date;
    }
}

export interface ICustomer extends IAbstractInterface {
    gender: ICustomer.Gender;
    last_name: string;
    first_name: string;
    email: string;
    additional_emails: ICustomer.IEmail[];
    phone: string;
    mobile: string;
    dob: Date;
    customer_number?: string;
    address: ICustomer.IAddress;
    bookings?: IBooking[];
    invoice_email?: string;
    customer_last_modified: Date;
    sub_type: ICustomer.SubType;
    point_status: IPointAccount.Status;
    com_channels: ICustomer.IAdditionalAttribute[];
    customer_properties: ICustomer.IAdditionalAttribute[];
    external_references: ICustomer.IAdditionalAttribute[];
}


