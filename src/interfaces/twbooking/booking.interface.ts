import {IPayment} from './payment.interface'
import IAbstractInterface from '../abstract.interface';
import {IPaymentReminder} from './paymentReminder.interface';
import {ICustomer} from './customer.interface';

export interface IBooking extends IAbstractInterface {
    booking_number: string;
    last_modified: Date;
    booking_status: number;
    payment_status: number;
    product_code: string;
    web_code: string;
    program_code: string;
    program_name: string;
    program_start: Date;
    program_end: Date;
    program_duration: number; // in days
    program_country: string;
    program_city: IBooking.IProgramCity;
    payments: IPayment[];
    payment_reminders: IPaymentReminder[];
    insurance_type: string;
    insurance_start: Date;
    insurance_end: Date;
    travel_begin: string;
    customer?: string | ICustomer;
    payment: IBooking.IBookingPayment;
    booking_properties: IBooking.IBookingProperty[];
    last_modified_details: IBooking.IBookingLastModifiedDetails;
    booking_notes: IBooking.IBookingNote[];
}

export namespace IBooking {
    export interface IProgramCity extends IAbstractInterface {
        name: string;
        lat: number;
        lng: number;
    }

    export interface IBookingPayment extends IAbstractInterface {
        open_payment_amount: number;
        payments: IPayment[];
    }

    export interface IBookingProperty extends IAbstractInterface {
        value: any;
        code: string;
        type: number;
        title: string;
        last_modified: Date;
    }

    export interface IBookingLastModifiedDetails extends IAbstractInterface {
        user_name: string;
        user_email: string;
        last_payment_date: Date;
        last_booking_property_date: Date;
    }

    export interface IBookingNote extends IAbstractInterface {
        content: string;
    }
}
