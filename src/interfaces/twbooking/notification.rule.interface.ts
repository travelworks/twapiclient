import IAbstractInterface from '../abstract.interface';

export interface INotificationRule extends IAbstractInterface {
    name: string;
    email_template: string;
    conditions: INotificationRule.ICondition[];
    active: boolean;
}

export namespace INotificationRule {
    export interface ICondition {
        target: string;
        type: ICondition.Type
        operator: ICondition.Operator;
        value: any;
    }
    export namespace ICondition {
        export enum Type {
            string = 'string',
            date = 'date'
        }
        export enum Operator {
            eq = 'eq',
            gte = 'gte',
            lte = 'lte',
            in = 'in',
            nin = 'nin',
            ne = 'ne'
        }
    }

}
