import IAbstractInterface from '../abstract.interface';

export interface IPayment extends IAbstractInterface {
    payment_id: string;
    open_payment_amount: number;
    payment_amount: number;
    payment_date: Date;
    email_send: boolean;
    email_id: string;
}
