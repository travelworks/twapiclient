import IAbstractInterface from '../abstract.interface';

export interface ILog extends IAbstractInterface {
    shared_field_name: string;
    old_value: string;
    new_value: string;
    timestamp: Date;
}
