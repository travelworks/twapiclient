import IAbstractInterface from "../abstract.interface";
import { IDeserializable } from "../deserializable.interface";


export namespace ISharedField {
    export interface IField extends IAbstractInterface {
        _id: string;
        name: string;
        shared: boolean;
        value: any;
        config: IConfig;
        timestamp: Date;
        _previousShared: boolean;
    }
    
    export interface IConfig extends IDeserializable<IConfig> {
        name: string;
        shared: boolean;
        import_function: boolean;
        title: any;
    }
}