import IAbstractInterface from "../abstract.interface";

export interface IGroup extends IAbstractInterface {
    _id: string,
    name: string,
    countries: string[],
    products: string[],
    show_map: boolean,
}