import IAbstractInterface from '../abstract.interface';

export interface IPointActivity extends IAbstractInterface {
    name: string;
    amount: number;
}
