import IAbstractInterface from '../abstract.interface';
import {IQuestion} from './question.interface';

export interface ISurvey extends IAbstractInterface {
    name: string;
    type: string;
    condition?: ISurvey.ICondition;
    mailSettings?: ISurvey.IMailSettings;
    questions?: IQuestion[];
    active?: boolean;
    thankYouText?: string;
    surveys: any;
    ttl: number;
    responses: ISurvey[];
}

export namespace ISurvey {
    export interface ICondition {
        start?: number;
        end?: number;
        products: string[];
        programs: string[];
    }

    export interface IMailSettings {
        firstTemplate: string;
        secondTemplate: string;
        remindingDays: number;
    }
}
