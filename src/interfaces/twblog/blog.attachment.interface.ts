import IAbstractInterface from '../abstract.interface';
import IAbstractTimeStampInterface from '../abstract.timestamp.interface';
import {IBlogAccount} from './blog.account.interface';
import {IBlogEntry} from './blog.entry.interface';


export interface IBlogAttachment extends IAbstractInterface, IAbstractTimeStampInterface {
    meta: any;
    link: string;
    blog_account: string;
    booking: IBlogEntry.IBlogBooking
    customer: IBlogAccount.IBlogCustomer;
    eTag: string;
    put_url: string; // not persistent
}
