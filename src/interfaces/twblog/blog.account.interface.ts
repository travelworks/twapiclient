import IAbstractInterface from '../abstract.interface';
import IAbstractTimeStampInterface from '../abstract.timestamp.interface';

export interface IBlogAccount extends IAbstractInterface, IAbstractTimeStampInterface {
    customer: IBlogAccount.IBlogCustomer;
    hash?: string;
    notifications?: IBlogAccount.IBlogNotification[];
    user_name?: string;
}

export namespace IBlogAccount {
    export interface IBlogCustomer extends IAbstractInterface {
        customer_number: string;
        first_name: string;
        last_name: string;
    }
    export interface IBlogNotification extends IAbstractInterface {
        type: IBlogNotification.Type;
        email: string;
        createdAt: Date;
        updatedAt: Date;
    }
    export namespace IBlogNotification {
        export enum Type {
            invite = 'invite'
        }
    }
}
