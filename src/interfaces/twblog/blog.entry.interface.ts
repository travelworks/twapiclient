import IAbstractInterface from '../abstract.interface';
import IAbstractTimeStampInterface from '../abstract.timestamp.interface';
import {IBlogAccount} from './blog.account.interface';
import {IBlogAttachment} from './blog.attachment.interface';


export interface IBlogEntry extends IAbstractInterface, IAbstractTimeStampInterface {
    blog_account: string;
    deletion_request: Date;
    booking: IBlogEntry.IBlogBooking
    customer: IBlogAccount.IBlogCustomer;
    title: string;
    body: string;
    originator_confirmed: boolean;
    exploitation_right_confirmed: boolean;
    public: boolean;
    new: boolean;
    comment: string;
    media: string[];
    published: boolean;
    teaser_attachment: IBlogAttachment;
    imported: boolean;
}

export namespace IBlogEntry {
    export interface IBlogBooking extends IAbstractInterface {
        booking_number: string;
        program_code: string;
        product_code: string;
        program_name: string;
        program_start: Date;
        program_end: Date;
        program_country: string;
    }
}
