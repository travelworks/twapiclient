import TwAbstractApiClient from './twAbstractApiClient';
import {CEmailTemplate} from '../classes/twemail/emailTemplate.class';
import {CEmail} from '../classes/twemail/email.class';


export namespace TwEmailClient {

    export class EmailClient<T = CEmail> extends TwAbstractApiClient<T> {
        protected readonly path = 'emails';
        Class = CEmail;
    }

    export class EmailTemplateClient<T = CEmailTemplate> extends TwAbstractApiClient<T> {
        protected readonly path = 'emailtemplates';
        Class = CEmailTemplate;
    }
}


