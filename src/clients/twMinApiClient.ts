import {ITwAbstractApiClient} from './twAbstractApiClient';
import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import CAbstractClass from '../classes/abstract.class';

export default abstract class TwMinApiClient<T> {

    private uri: string;
    protected path: string;
    private key_id: string | null;
    private key_secret: string | null;
    protected tokenFunction: any | null;
    protected Class: typeof CAbstractClass;

    constructor(uri: string, key_id?: string | null, key_secret?: string | null, tokenFunction?: any | null) {
        if (new.target === TwMinApiClient) {
            throw new TypeError('Cannot construct Abstract instances directly');
        }
        this.uri = uri;
        this.key_id = key_id;
        this.key_secret = key_secret;
        this.tokenFunction = tokenFunction;
        this.addInterceptor();
    }

    private addInterceptor() {
        axios.interceptors.request.use(async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
            if (this.tokenFunction) {
                const token = await this.tokenFunction();
                config.headers.Authorization = token;
            }
            return config;
        });
    }

    protected getOptions(path: string | null = null): AxiosRequestConfig {
        let headers: any;
        headers = {'content-type': 'application/json'};
        if (this.key_id && this.key_secret) {
            headers.authorization = JSON.stringify({
                KEY_ID: this.key_id,
                KEY_SECRET: this.key_secret
            });
        }
        path = path || this.path;
        const axiosRequestConfig: AxiosRequestConfig = {
            url: this.getUir() + path + '/',
            headers: headers
        };
        return axiosRequestConfig;
    }

    protected getUir() {
        return this.uri.replace(/\/?$/, '/');
    }

    protected prepareResult(res: AxiosResponse): T | any {
        if (this.Class) {
            const object: T = new this.Class().deserialize(res.data);
            return (object);
        } else {
            return (res.data);
        }
    }
}
