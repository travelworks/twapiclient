import {CCustomer} from '../classes/twbooking/customer.class';
import {CBooking} from '../classes/twbooking/booking.class';
import TwAbstractApiClient, {ITwListResult} from './twAbstractApiClient';
import {CNotificationRule} from '../classes/twbooking/notification.rule.class';
import axios, {AxiosResponse} from 'axios';
import {CReminder} from '../classes/twbooking/reminder.class';
import {CNotificationBlock} from '../classes/twbooking/notification.block.class';
import {CNotification} from '../classes/twbooking/notification.class';
import TwMinApiClient from './twMinApiClient';
import {INotificationBlock} from '..';


export namespace TwBookingClient {

    export class CustomerClient<T = CCustomer> extends TwAbstractApiClient<T> {
        protected readonly path = 'customers';
        Class = CCustomer;

        getBookings(query: any = null, sorting: any = null, paging: any = null): Promise<ITwListResult<any>> {
            return new Promise((resolve, reject) => {
                query['bookings.0'] = {$exists: true};
                super.list(query, sorting, paging).then((customerResponse) => {
                    const customerBookings: any = []
                    customerResponse.items.forEach((customer: any) => {
                        customer.bookings.forEach((booking: CBooking) => {
                            customerBookings.push({customer: customer, booking: booking});
                        })
                    });
                    resolve({items: customerBookings, totalCount: customerResponse.totalCount});
                });
            });
        }
    }

    export namespace CustomerClient {

        class AbstractCustomerClient<T> extends TwAbstractApiClient<T> {
            protected readonly path = 'customers';
            protected readonly subPath: string;

            list(customer_id: string, query: any = null, sorting: any = null, paging: any = null, projection: any = null):
                Promise<ITwListResult<T>> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return super.list(query, sorting, paging, projection, path);
            }

            get(customer_id: string, id: string): Promise<T> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return super.get(id, path);
            }

            post(customer_id: string, data: any): Promise<T> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return super.post(data, path);
            }

            patch(customer_id: string, data: any, id?: string): Promise<T> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return super.patch(data, id, path);
            }

            put(customer_id: string, data: any, id?: string): Promise<T> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return super.put(data, id, path);
            }

            delete(customer_id: string, id: string): Promise<T> {
                const path = this.path + '/' + customer_id.toString() + '/' + this.subPath
                return super.delete(id, path);
            }
        }

        export class NotificationClient<T = CNotification> extends AbstractCustomerClient<T> {
            protected readonly subPath = 'notifications';
            Class = CNotification;
        }


        export class NotificationBlockClient<T = CNotificationBlock> extends TwMinApiClient<T> {
            Class = CNotificationBlock;

            static getBlockedStatus(block: INotificationBlock, scope: NotificationBlockClient.Scopes, notification?: string): boolean {
                if (!block) {
                    return false;
                }
                if (block.block_all) {
                    return true;
                }
                switch (scope) {
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.survey:
                        return block.block_surveys;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.companion:
                        return block.block_companions;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.point:
                        return block.block_points;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.payment:
                        return block.block_payments;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.notification:
                        if (block.notification_rules
                            && block.notification_rules.length
                            && block.notification_rules.includes(notification)) {
                            return true;
                        }
                        return false;
                        break;
                    default:
                        return false;
                }
            }

            get(cid: string): Promise<CNotificationBlock> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'GET';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            patch(cid: string, data: any): Promise<CNotificationBlock> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'PATCH';
                    options.data = data;
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            delete(cid: string): Promise<CNotificationBlock> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'DELETE';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            async isBlocked(cid: string, scope: NotificationBlockClient.Scopes, notification?: string): Promise<boolean> {
                try {
                    const block: CNotificationBlock = await this.get(cid);
                    return NotificationBlockClient.getBlockedStatus(block, scope, notification);
                } catch (e) {
                    return false;
                }
            }


        }

        export namespace NotificationBlockClient {

            export enum Scopes {
                survey = 'survey',
                companion = 'companion',
                point = 'point',
                payment = 'payment',
                notification = 'notification'
            }
        }

        export class NoteClient<T = CCustomer.CNote> extends AbstractCustomerClient<T> {
            protected readonly subPath = 'notes';
            Class = CCustomer.CNote;
        }

        // tslint:disable-next-line:no-shadowed-variable
        export class ReminderClient<T = CReminder> extends AbstractCustomerClient<T> {
            protected readonly subPath = 'reminders';
            Class = CReminder;
        }
    }

    export class ReminderClient<T = CReminder> extends TwAbstractApiClient<T> {
        protected readonly path = 'reminders';
        Class = CReminder;
    }

    export class NotificationRuleClient<T = CNotificationRule> extends TwAbstractApiClient<T> {
        protected readonly path = 'notificationrules';
        Class = CNotificationRule;

        async test(id: string): Promise<CBooking[]> {
            try {
                const options = this.getOptions();
                options.url += id + '/test';
                options.method = 'get';
                const res: AxiosResponse = await axios(options);
                const result: CBooking[] = CBooking.deserialize(res.data);
                return result;
            } catch (e) {
                throw e;
            }
        }
    }
}


