import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import TwMinApiClient from './twMinApiClient';


interface Header {
    'content-type': string,
    authorization?: string,
}

interface Option {
    json: string,
    headers: Header,
    method: string,
    uri: string,
    body?: any,
}

export interface ITwListResult<T> {
    items: T[];
    totalCount: number;
}

export interface ITwAbstractApiClient<T> {
    list(query: any | null, sorting: any | null, paging: any | null, projection: any | null): Promise<ITwListResult<T>>;

    get(id: string): Promise<T>;

    post(data: any): Promise<T>;

    patch(data: any, id?: string): Promise<T>;

    put(data: any, id?: string): Promise<T>;

    delete(id: string): Promise<T>;
}

export default class TwAbstractApiClient<T> extends TwMinApiClient<T> implements ITwAbstractApiClient<T> {


    list(query: any | null = null,
         sorting: any | null = null,
         paging: any | null = null,
         projection: any | null = null,
         path: string | null = null): Promise<ITwListResult<T>> {

        let queryPath = '';
        if (query || sorting || paging) {
            queryPath += '?';
            const params = [];
            if (Object.keys(query).length) {
                params.push('q=' + encodeURIComponent(JSON.stringify(query)));
            }
            if (sorting) {
                params.push('s=' + encodeURIComponent(JSON.stringify(sorting)));
            }
            if (paging) {
                params.push('p=' + encodeURIComponent(JSON.stringify(paging)));
            }
            if (projection) {
                params.push('o=' + encodeURIComponent(JSON.stringify(projection)));
            }
            queryPath += params.join('&');
        }

        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            options.method = 'get';
            options.url += queryPath;
            axios(options).then((res: AxiosResponse) => {
                const twListResult: ITwListResult<T> = {
                    items: res.data,
                    totalCount: parseInt(res.headers['x-total-count'],10) || 0
                }
                if (this.Class) {
                    twListResult.items = this.Class.deserialize(res.data);
                }
                resolve(twListResult);
            }).catch((err: any) => {
                reject(err)
            });
        });
    }

    get(id: string, path: string | null = null): Promise<T> {
        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            options.url += id;
            options.method = 'get';
            axios(options).then((res: AxiosResponse) => {
                resolve(this.prepareResult(res));
            }).catch((err: any) => {
                reject(err)
            });
        });
    }

    post(data: any, path: string | null = null): Promise<T> {
        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            options.method = 'POST';
            options.data = data;
            axios(options).then((res: AxiosResponse) => {
                resolve(this.prepareResult(res));
            }).catch((err: any) => {
                reject(err)
            });
        });
    }

    patch(data: any, id?: string, path: string | null = null): Promise<T> {
        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            id = (id) ? id : data._id;
            options.url += id;
            options.method = 'PATCH';
            options.data = data;
            axios(options).then((res: AxiosResponse) => {
                resolve(this.prepareResult(res));
            }).catch((err: any) => {
                reject(err)
            });
        });
    }

    put(data: any, id?: string, path: string | null = null): Promise<T> {
        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            id = (id) ? id : data._id;
            options.url += id;
            options.method = 'PUT';
            options.data = data;
            axios(options).then((res: AxiosResponse) => {
                resolve(this.prepareResult(res));
            }).catch((err: any) => {
                reject(err)
            });
        });
    }

    delete(id: string, path: string | null = null): Promise<T> {
        return new Promise((resolve, reject) => {
            const options = this.getOptions(path);
            options.url += id;
            options.method = 'DELETE';
            axios(options).then((res: AxiosResponse) => {
                resolve(this.prepareResult(res));
            }).catch((err: any) => {
                reject(err)
            });
        });
    }


}

