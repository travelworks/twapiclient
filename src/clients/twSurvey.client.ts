import TwAbstractApiClient from './twAbstractApiClient';
import {CSurvey} from '../classes/twsurvey/survey.class';
import {CRequest} from '../classes/twsurvey/request.class';
import {CQuestion} from '../classes/twsurvey/question.class';
import TwMinApiClient from './twMinApiClient';
import axios, {AxiosResponse} from 'axios';

export namespace TwSurveyClient {

    export namespace Private {
        export class SurveyClient<T = CSurvey> extends TwAbstractApiClient<T> {
            protected readonly path = 'surveys';
            Class = CSurvey;
        }

        export class RequestClient<T = CRequest> extends TwAbstractApiClient<T> {
            protected readonly path = 'requests';
            Class = CRequest;
        }

        export class QuestionClient<T = CQuestion> extends TwAbstractApiClient<T> {
            protected readonly path = 'questions';
            Class = CQuestion;
        }
    }

    export namespace Public {
        export class SurveyClient<T = CSurvey> extends TwMinApiClient<T> {
            protected readonly path = 'public/surveys';
            Class = CSurvey;

            get(id: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += id;
                    options.method = 'get';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }
        }

        export class RequestClient<T = CRequest>  extends TwMinApiClient<T> {
            protected readonly path = 'public/requests';
            Class = CRequest;

            get(hash: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            put(data: any, id?: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    id = (id) ? id : data._id;
                    options.url += id;
                    options.method = 'PUT';
                    options.data = data;
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }
        }

    }
}


