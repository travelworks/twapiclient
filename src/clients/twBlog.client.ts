import TwAbstractApiClient from './twAbstractApiClient';
import axios, {AxiosResponse} from 'axios';
import TwMinApiClient from './twMinApiClient';
import {CBlogAccount} from '../classes/twblog/blog.account.class';
import {CBlogEntry} from '../classes/twblog/blog.entry.class';
import {CBlogAttachment} from '../classes/twblog/blog.attachment.class';

export namespace TwBlogClient {
    export namespace Private {
        export class BlogAccountClient<T = CBlogAccount> extends TwAbstractApiClient<T> {
            protected readonly path = 'accounts';
            Class = CBlogAccount;
        }

        export class BlogEntryClient<T = CBlogEntry> extends TwAbstractApiClient<T> {
            protected readonly path = 'entries';
            Class = CBlogEntry;
        }

        export class BlogAttachmentClient<T = CBlogAttachment> extends TwAbstractApiClient<T> {
            protected readonly path = 'attachments';
            Class = CBlogAttachment;

            async addAttachment(attachment: CBlogAttachment, file: any): Promise<CBlogAttachment> {
                try {
                    const options = this.getOptions();
                    options.method = 'post';
                    options.data = attachment;
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogAttachment = new CBlogAttachment().deserialize(res.data);
                    if (result && result.put_url) {
                        const url = new URL(result.put_url);
                        const x_amz_acl = url.searchParams.get('x-amz-acl');
                        const x_amz_tagging = url.searchParams.get('x-amz-tagging');
                        const putResult = await axios.put(result.put_url, file, {
                            headers: {
                                'x-amz-acl': x_amz_acl,
                                'x-amz-tagging': x_amz_tagging,
                                'Content-Type': attachment.meta.type
                            }
                        });
                    }
                    return result;
                } catch (e) {
                    throw e;
                }
            }
        }
    }
    export namespace User {
        import CBlogBooking = CBlogEntry.CBlogBooking;

        export class BlogAccountClient<T = CBlogAccount> extends TwMinApiClient<T> {
            protected readonly path = 'public/accounts';
            Class = CBlogAccount;

            get(hash: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            async getEntries(hash: string): Promise<CBlogEntry[]> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/entries';
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry[] = CBlogEntry.deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async addEntry(hash: string, entry: CBlogEntry): Promise<CBlogEntry> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/entries';
                    options.method = 'post';
                    options.data = entry;
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry = new CBlogEntry().deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async updateEntry(hash: string, eid: string, entry: CBlogEntry): Promise<CBlogEntry> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/entries/' + eid;
                    options.method = 'put';
                    options.data = entry;
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry = new CBlogEntry().deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async deleteEntry(hash: string, eid: string): Promise<CBlogEntry> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/entries/' + eid;
                    options.method = 'delete';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry = new CBlogEntry().deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async getEntry(hash: string, eid: string): Promise<CBlogEntry> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/entries/' + eid;
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry = new CBlogEntry().deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async setUserName(hash: string, user_name: string): Promise<boolean> {
                try {
                    const options = this.getOptions();
                    options.url += hash;
                    options.method = 'put';
                    options.data = {user_name};
                    await axios(options);
                    return true;
                } catch (e) {
                    throw e;
                }
            }

            async addAttachment(hash: string, attachment: CBlogAttachment, file: any): Promise<CBlogAttachment> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/attachments';
                    options.method = 'post';
                    options.data = attachment;
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogAttachment = new CBlogAttachment().deserialize(res.data);
                    if (result && result.put_url) {
                        const url = new URL(result.put_url);
                        const x_amz_acl = url.searchParams.get('x-amz-acl');
                        const x_amz_tagging = url.searchParams.get('x-amz-tagging');
                        const axiosInstance = axios.create();
                        const putResult = await axiosInstance.put(result.put_url, file, {
                            headers: {
                                'x-amz-acl': x_amz_acl,
                                'x-amz-tagging': x_amz_tagging,
                                'Content-Type': attachment.meta.type
                            }
                        });
                    }
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async getAttachments(hash: string): Promise<CBlogAttachment[]> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/attachments';
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogAttachment[] = CBlogAttachment.deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async getBookings(hash: string): Promise<CBlogEntry.CBlogBooking[]> {
                try {
                    const options = this.getOptions();
                    options.url += hash + '/bookings';
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry.CBlogBooking[] = [];
                    for (const b of res.data) {
                        const object: CBlogBooking = new CBlogBooking();
                        result.push(object.deserialize(b));
                    }
                    return result;
                } catch (e) {
                    throw e;
                }
            }
        }
    }
    export namespace Public {
        export class BlogEntryClient<T = CBlogEntry> extends TwMinApiClient<T> {
            protected readonly path = 'public/blogs';

            async getEntries(user_name: string): Promise<CBlogEntry[]> {
                try {
                    const options = this.getOptions();
                    options.url += user_name + '/entries';
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry[] = CBlogEntry.deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }

            async getEntry(user_name: string, eid: string): Promise<CBlogEntry> {
                try {
                    const options = this.getOptions();
                    options.url += user_name + '/entries/' + eid;
                    options.method = 'get';
                    const res: AxiosResponse = await axios(options);
                    const result: CBlogEntry = new CBlogEntry().deserialize(res.data);
                    return result;
                } catch (e) {
                    throw e;
                }
            }
        }
    }
}
