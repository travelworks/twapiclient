import axios, {AxiosResponse} from 'axios';
import TwAbstractApiClient from './twAbstractApiClient';
import {CPointAccount} from '../classes/twpoint/point.account.class';
import {CPointActivity} from '../classes/twpoint/point.activity.class';
import TwMinApiClient from './twMinApiClient';

export namespace TwPointClient {
    export namespace Private {

        import CPointEntry = CPointAccount.CPointEntry;

        export class PointAccountClient<T = CPointAccount> extends TwAbstractApiClient<T> {
            protected readonly path = 'accounts';
            Class = CPointAccount;

            postEntry(entry: CPointAccount.CPointEntry, customerID: string): Promise<CPointEntry> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += customerID + '/entries';
                    options.method = 'POST';
                    options.data = entry;
                    axios(options).then((res: AxiosResponse) => {
                        resolve(res.data);
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            deleteEntry(entryID: string, customerID: string): Promise<CPointEntry> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += customerID + '/entries/' + entryID;
                    options.method = 'DELETE';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(res.data);
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }
        }

        export class PointActivityClient<T = CPointActivity> extends TwAbstractApiClient<T> {
            protected readonly path = 'activities';
            Class = CPointActivity;
        }
    }

    export namespace Public {

        export class PointAccountClient<T = CPointAccount> extends TwMinApiClient<T> {
            protected readonly path = 'public/accounts';
            Class = CPointAccount;

            get(hash: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }

            postStatus(status: any, hash: string): Promise<T> {
                return new Promise((resolve, reject) => {
                    const options = this.getOptions();
                    options.url += hash + '/status';
                    options.method = 'POST';
                    options.data = {status: status};
                    axios(options).then((res: AxiosResponse) => {
                        resolve(this.prepareResult(res));
                    }).catch((err: any) => {
                        reject(err)
                    });
                });
            }
        }
    }
}
