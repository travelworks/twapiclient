import {CCompanion} from '../classes/twcompanion/companion.class';
import {CGroup} from '../classes/twcompanion/group.class';
import TwAbstractApiClient from './twAbstractApiClient';

export namespace TwCompanionClient {
    export namespace Private {
        export class CompanionClient<T = CCompanion> extends TwAbstractApiClient<T> {
            protected readonly path = 'companions';
            Class = CCompanion;
        }
        export class GroupClient<T = CGroup> extends TwAbstractApiClient<T> {
            protected readonly path = 'groups';
            Class = CGroup;
        }
    }
}
