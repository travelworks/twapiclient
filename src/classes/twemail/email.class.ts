import CAbstractClass from '../abstract.class';
import {IEmail} from '../..';
import {CAttachment, CEmailTemplate} from './emailTemplate.class';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class CEmail extends CAbstractClass implements IEmail, IDeserializable<CEmail> {
    attachments: CAttachment[];
    bcc: string[];
    cc: string[];
    emailTemplate: CEmailTemplate;
    from: string;
    html: string;
    placeholders: IEmail.IPlaceholders;
    send_status: IEmail.ISendStatus;
    subject: string;
    text: string;
    to: string[];
    createdAt: Date;

    deserialize(input: IEmail): CEmail {
        super.deserialize(input);
        this.attachments = [];
        if (input.attachments && input.attachments.length) {
            input.attachments.forEach((attachment: any) => {
                this.attachments.push(new CAttachment().deserialize(attachment));
            })
        }
        this.bcc = input.bcc;
        this.cc = input.cc;
        if (input.emailTemplate) {
            this.emailTemplate = new CEmailTemplate().deserialize(input.emailTemplate);
        }
        this.from = input.from;
        this.html = input.html;
        this.placeholders = input.placeholders;
        this.send_status = input.send_status;
        this.subject = input.subject;
        this.text = input.text;
        this.to = input.to;
        this.createdAt = input.createdAt;
        return this;
    }


}
