import CAbstractClass from '../abstract.class';
import {IAttachment, IEmailTemplate} from '../../index';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class CEmailTemplate extends CAbstractClass implements IEmailTemplate, IDeserializable<CEmailTemplate> {
    attachments: CAttachment[];
    from: string;
    html: string;
    name: string;
    placeholders: IEmailTemplate.IPlaceholder[];
    subject: string;
    text: string;
    type: string;

    deserialize(input: IEmailTemplate): CEmailTemplate {
        super.deserialize(input);
        this.attachments = [];
        if (input.attachments && input.attachments.length) {
            input.attachments.forEach((attachment: any) => {
                this.attachments.push(new CAttachment().deserialize(attachment));
            })
        }
        this.from = input.from;
        this.html = input.html;
        this.name = input.name;
        this.placeholders = input.placeholders;
        this.subject = input.subject;
        this.text = input.text;
        this.type = input.type;
        return this;
    }


}

export class CAttachment extends CAbstractClass implements IAttachment, IDeserializable<CAttachment> {
    data: any;
    meta: any;

    deserialize(input: any): CAttachment {
        super.deserialize(input);
        this.data = input.data;
        this.meta = input.meta;
        return this;
    }

}
