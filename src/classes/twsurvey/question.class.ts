import CAbstractClass from '../abstract.class';
import {IQuestion} from '../../interfaces/twsurvey/question.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {COption} from './option.class';
import {IOption} from '../../interfaces/twsurvey/option.interface';

export class CQuestion extends CAbstractClass implements IQuestion, IDeserializable<CQuestion> {
    answer: any;
    conditions: COption[];
    input_type: IQuestion.InputTypes;
    keyword: string;
    limit: number;
    name: string;
    options: COption[];
    required: boolean;
    text: string;
    type: IQuestion.Types;
    questions: CQuestion[];

    deserialize(input: IQuestion): CQuestion {
        super.deserialize(input);
        this.conditions = [];
        if (input.conditions && input.conditions.length) {
            input.conditions.forEach((condition: IOption) => {
                this.conditions.push(new COption().deserialize(condition));
            });
        }
        this.input_type = input.input_type;
        this.keyword = input.keyword;
        this.limit = input.limit;
        this.name = input.name;
        this.options = [];
        if (input.options && input.options.length) {
            input.options.forEach((option: IOption) => {
                this.options.push(new COption().deserialize(option));
            });
        }
        this.required = input.required;
        this.text = input.text;
        this.type = input.type;
        this.answer = input.answer;
        return this;
    }

    getAnswer(label: boolean = false): string {
        switch (this.type) {
            case 'choice':
                if (label) {
                    return this.answer.label;
                } else {
                    return this.answer.value;
                }
            case 'multichoice':
                if (label) {
                    return this.answer.map((option: COption) => option.label).join(', ');
                } else {
                    return this.answer.map((option: COption) => option.value).join(', ');
                }
            case 'rating':
            case 'starrating':
                return ((this.answer / this.limit * 100).toFixed(1) + '%').replace('.', ',');
            default:
                return this.answer;

        }
    }

    conditionFulfilled(operator: any): Boolean {
        if (Array.isArray(operator)) {
            return operator.some(ov => this.conditions.includes(ov));
        } else {
            return this.conditions.includes(operator);
        }
    }
}
