import CAbstractClass from '../abstract.class';
import {ISurvey} from '../../interfaces/twsurvey/survey.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CQuestion} from './question.class';

export class CSurvey extends CAbstractClass implements ISurvey, IDeserializable<CSurvey> {
    active: boolean;
    condition: CSurvey.CCondition;
    mailSettings: CSurvey.CMailSettings;
    name: string;
    questions: CQuestion[];
    responses: CSurvey[];
    surveys: any;
    thankYouText: string;
    ttl: number;
    type: string;

    deserialize(input: ISurvey): CSurvey {
        super.deserialize(input);
        this.active = input.active;
        if (input.condition) {
            this.condition = new CSurvey.CCondition().deserialize(input.condition);
        }
        if (input.mailSettings) {
            this.mailSettings = new CSurvey.CMailSettings().deserialize(input.mailSettings);
        }
        this.name = input.name;
        this.questions = [];
        if (input.questions && input.questions.length) {
            input.questions.forEach((question: any) => {
                this.questions.push(new CQuestion().deserialize(question));
            });
        }
        this.responses = [];
        if (input.responses && input.responses.length) {
            input.responses.forEach((response: any) => {
                this.responses.push(new CSurvey().deserialize(response));
            });
        }
        this.surveys = [];
        if (input.surveys && input.surveys.length) {
            input.surveys.forEach((survey: any) => {
                this.surveys.push(new CSurvey().deserialize(survey));
            });
        }
        this.thankYouText = input.thankYouText;
        this.ttl = input.ttl;
        this.type = input.type;
        return this;
    }
}

export namespace CSurvey {

    export class CCondition implements ISurvey.ICondition, IDeserializable<CCondition> {
        end: number;
        products: string[];
        programs: string[];
        start: number;

        deserialize(input: any): CCondition {
            this.end = input.end;
            this.products = [];
            if (input.products && input.products.length) {
                this.products = input.products;
            }
            this.programs = [];
            if (input.programs && input.programs.length) {
                this.programs = input.programs;
            }
            this.start = input.start;
            return this;
        }
    }

    export class CMailSettings implements ISurvey.IMailSettings, IDeserializable<CMailSettings> {
        firstTemplate: string;
        remindingDays: number;
        secondTemplate: string;

        deserialize(input: any): CSurvey.CMailSettings {
            this.firstTemplate = input.firstTemplate;
            this.remindingDays = input.remindingDays;
            this.secondTemplate = input.secondTemplate;
            return this;
        }

    }
}
