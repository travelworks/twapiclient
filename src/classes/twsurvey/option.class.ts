import CAbstractClass from '../abstract.class';
import {IOption} from '../../interfaces/twsurvey/option.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class COption extends CAbstractClass implements IOption, IDeserializable<COption> {
    label: string;
    value: string;

    deserialize(input: IOption): COption {
        super.deserialize(input);
        this.label = input.label;
        this.value = input.value;
        return this;
    }

}
