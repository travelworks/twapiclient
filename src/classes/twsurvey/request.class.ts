import CAbstractClass from '../abstract.class';
import {IRequest} from '../../interfaces/twsurvey/request.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CBooking} from '../twbooking/booking.class';
import {CCustomer} from '../twbooking/customer.class';
import {IBooking} from '../../interfaces/twbooking/booking.interface';
import {ICustomer} from '../../interfaces/twbooking/customer.interface';

import {CSurvey} from './survey.class';

export class CRequest extends CAbstractClass implements IRequest, IDeserializable<CRequest> {
    booking: any;
    customer: any;
    email_status: CRequest.CEMailStatus;
    hash: string;
    internal: CRequest.CInternal;
    is_new: boolean;
    response: CSurvey;
    status: string;
    survey: any;
    createdAt: Date;
    updatedAt: Date;

    deserialize(input: IRequest): CRequest {
        super.deserialize(input);
        this.booking = input.booking;
        this.customer = input.customer;
        if (input.email_status) {
            this.email_status = new CRequest.CEMailStatus().deserialize(input.email_status);
        }
        this.hash = input.hash;
        if (input.internal) {
            this.internal = new CRequest.CInternal().deserialize(input.internal);
        }
        this.is_new = input.is_new;
        if (input.response) {
            this.response = new CSurvey().deserialize(input.response);
        }
        this.status = input.status;
        this.survey = input.survey;
        this.createdAt = input.createdAt;
        this.updatedAt = input.updatedAt;
        return this;
    }

}

export namespace CRequest {

    export class CEMailStatus implements IRequest.IEMailStatus, IDeserializable<CEMailStatus> {
        firstEmail?: any;
        secondEmail?: any;

        deserialize(input: any): CRequest.CEMailStatus {
            this.firstEmail = input.firstEmail;
            this.secondEmail = input.secondEmail;
            return this;
        }


    }

    export class CInternal implements IRequest.IInternal, IDeserializable<CInternal> {
        customer: CCustomer;
        booking: CBooking;

        deserialize(input: any): CRequest.CInternal {
            this.customer = new CCustomer().deserialize(input.customer);
            this.booking = new CBooking().deserialize(input.booking);

            return this;
        }

    }
}
