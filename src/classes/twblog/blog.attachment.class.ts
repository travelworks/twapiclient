import {CAbstractTimestampClass} from '../abstract.timestamp.class';
import {IBlogAttachment} from '../../interfaces/twblog/blog.attachment.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {IBlogEntry} from '../../interfaces/twblog/blog.entry.interface';
import {IBlogAccount} from '../../interfaces/twblog/blog.account.interface';
import {CBlogEntry} from './blog.entry.class';


export class CBlogAttachment extends CAbstractTimestampClass implements IBlogAttachment, IDeserializable<CBlogAttachment> {
    link: string;
    meta: any;
    blog_account: string;
    booking: IBlogEntry.IBlogBooking;
    customer: IBlogAccount.IBlogCustomer;
    eTag: string;
    put_url: string;

    deserialize(input: IBlogAttachment): any {
        super.deserialize(input);
        this.link = input.link;
        this.meta = input.meta;
        this.blog_account = input.blog_account;
        this.booking = new CBlogEntry.CBlogBooking().deserialize(input.booking);
        this.customer = input.customer;
        this.eTag = input.eTag;
        this.put_url = input.put_url;
        return this;
    }
}
