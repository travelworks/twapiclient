import {CAbstractTimestampClass} from '../abstract.timestamp.class';
import {IBlogAccount} from '../../interfaces/twblog/blog.account.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';


export class CBlogAccount extends CAbstractTimestampClass implements IBlogAccount, IDeserializable<CBlogAccount> {
    customer: IBlogAccount.IBlogCustomer;
    hash: string;
    notifications: IBlogAccount.IBlogNotification[];
    user_name: string;

    deserialize(input: IBlogAccount): any {
        super.deserialize(input);
        this.customer = input.customer;
        this.hash = input.hash;
        this.notifications = input.notifications;
        this.user_name = input.user_name;
        return this;
    }
}
