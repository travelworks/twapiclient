import {CAbstractTimestampClass} from '../abstract.timestamp.class';
import {IBlogEntry} from '../../interfaces/twblog/blog.entry.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CBlogAttachment} from './blog.attachment.class';
import {IBlogAccount} from '../../interfaces/twblog/blog.account.interface';
import CAbstractClass from '../abstract.class';


export class CBlogEntry extends CAbstractTimestampClass implements IBlogEntry, IDeserializable<CBlogEntry> {
    blog_account: string;
    body: string;
    booking: IBlogEntry.IBlogBooking;
    customer: IBlogAccount.IBlogCustomer;
    comment: string;
    deletion_request: Date;
    exploitation_right_confirmed: boolean;
    media: string[];
    new: boolean;
    originator_confirmed: boolean;
    public: boolean;
    title: string;
    published: boolean;
    teaser_attachment: CBlogAttachment;
    imported = false;

    deserialize(input: IBlogEntry): any {
        super.deserialize(input);
        this.blog_account = input.blog_account;
        this.body = input.body;
        this.booking = new CBlogEntry.CBlogBooking().deserialize(input.booking);
        this.customer = input.customer;
        this.comment = input.comment;
        this.deletion_request = input.deletion_request;
        this.exploitation_right_confirmed = input.exploitation_right_confirmed;
        this.media = input.media;
        this.new = input.new;
        this.originator_confirmed = input.originator_confirmed;
        this.public = input.public;
        this.title = input.title;
        this.published = input.published;
        if (input.teaser_attachment) {
            this.teaser_attachment = new CBlogAttachment().deserialize(input.teaser_attachment);
        }
        this.imported = !!input.imported;
        return this;
    }
}

export namespace CBlogEntry {
    export class CBlogBooking extends CAbstractClass implements IBlogEntry.IBlogBooking, IDeserializable<CBlogBooking> {
        booking_number: string;
        product_code: string;
        program_code: string;
        program_country: string;
        program_end: Date;
        program_name: string;
        program_start: Date;

        deserialize(input: IBlogEntry.IBlogBooking): any {
            super.deserialize(input);
            this.booking_number = input.booking_number;
            this.program_code = input.program_code;
            this.product_code = input.product_code;
            this.program_country = input.program_country;
            this.program_end = input.program_end;
            this.program_name = input.program_name;
            this.program_start = input.program_start;
            return this;
        }
    }
}
