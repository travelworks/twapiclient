import {INotificationBlock} from '../../interfaces/twbooking/notification.block.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CAbstractTimestampClass} from '../abstract.timestamp.class';


export class CNotificationBlock extends CAbstractTimestampClass implements INotificationBlock, IDeserializable<CNotificationBlock> {
    notification_rules: string[];
    customer: string;
    block_all: boolean;
    block_companions: boolean;
    block_points: boolean;
    block_surveys: boolean;
    block_payments: boolean;
    block_notifications: boolean;

    deserialize(input: INotificationBlock): CNotificationBlock {
        super.deserialize(input);
        this.notification_rules = input.notification_rules;
        this.customer = input.customer;
        this.block_all = input.block_all;
        this.block_companions = input.block_companions;
        this.block_points = input.block_points;
        this.block_surveys = input.block_surveys;
        this.block_payments = input.block_payments;
        this.block_notifications = input.block_notifications;
        return this;
    }
}
