import CAbstractClass from '../abstract.class';
import {IBooking} from '../../interfaces/twbooking/booking.interface';
import {IPayment} from '../../interfaces/twbooking/payment.interface';
import {IPaymentReminder} from '../../interfaces/twbooking/paymentReminder.interface';
import {CPaymentReminder} from './paymentReminder.class';
import {CPayment} from './payment.class';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import IProgramCity = IBooking.IProgramCity;
import {CCustomer} from './customer.class';

export class CBooking extends CAbstractClass implements IBooking, IDeserializable<CBooking> {
    booking_number: string;
    booking_status: number;
    last_modified: Date;
    payment_reminders: CPaymentReminder[];
    payment_status: number;
    payments: CPayment[];
    payment: CBooking.CBookingPayment;
    product_code: string;
    program_city: CBooking.CProgramCity;
    program_code: string;
    program_country: string;
    program_end: Date;
    program_duration: number;
    program_name: string;
    program_start: Date;
    web_code: string;
    insurance_type: string;
    insurance_start: Date;
    insurance_end: Date;
    travel_begin: string;
    booking_properties: CBooking.CBookingProperty[];
    last_modified_details: CBooking.CBookingLastModifiedDetails;
    booking_notes: CBooking.CBookingNote[];
    customer?: string | CCustomer;

    deserialize(input: IBooking): CBooking {
        super.deserialize(input);
        this.booking_number = input.booking_number;
        this.last_modified = input.last_modified;
        this.booking_status = input.booking_status;
        this.payment_status = input.payment_status;
        this.product_code = input.product_code;
        this.web_code = input.web_code;
        this.program_code = input.program_code;
        this.program_name = input.program_name;
        this.program_start = input.program_start;
        this.program_end = input.program_end;
        this.program_country = input.program_country;
        this.program_duration = input.program_duration;

        this.payments = [];
        if (input.payments && input.payments.length) {
            input.payments.forEach((payment: IPayment) => {
                this.payments.push(new CPayment().deserialize(payment));
            });
        }

        this.payment_reminders = [];
        if (input.payment_reminders && input.payment_reminders.length) {
            input.payment_reminders.forEach((payment_reminder: IPaymentReminder) => {
                this.payment_reminders.push(new CPaymentReminder().deserialize(payment_reminder));
            });
        }

        if (input.program_city) {
            this.program_city = new CBooking.CProgramCity().deserialize(input.program_city);
        }

        this.insurance_type = input.insurance_type;
        this.insurance_start = input.insurance_start;
        this.insurance_end = input.insurance_end;
        this.travel_begin = input.travel_begin;

        if (input.customer && typeof input.customer === 'object') {
            this.customer = new CCustomer().deserialize(input.customer);
        }

        if (input.payment) {
            this.payment = new CBooking.CBookingPayment().deserialize(input.payment);
        }

        this.booking_properties = [];
        if (input.booking_properties && input.booking_properties.length) {
            input.booking_properties.forEach((booking_property: IBooking.IBookingProperty) => {
                this.booking_properties.push(new CBooking.CBookingProperty().deserialize(booking_property));
            });
        }

        this.booking_notes = [];
        if (input.booking_notes && input.booking_notes.length) {
            input.booking_notes.forEach((booking_note: IBooking.IBookingNote) => {
                this.booking_notes.push(new CBooking.CBookingNote().deserialize(booking_note));
            });
        }

        if (input.last_modified_details) {
            this.last_modified_details = new CBooking.CBookingLastModifiedDetails().deserialize(input.last_modified_details);
        }

        return this;
    }
}

export namespace CBooking {
    export class CProgramCity extends CAbstractClass implements IProgramCity, IDeserializable<CProgramCity> {
        lat: number;
        lng: number;
        name: string;

        deserialize(input: any): CProgramCity {
            this.lat = input.lat;
            this.lng = input.lng;
            this.name = input.name;
            return this;
        }
    }

    export class CBookingPayment extends CAbstractClass implements IBooking.IBookingPayment, IDeserializable<CBookingPayment> {
        open_payment_amount: number;
        payments: CPayment[];

        deserialize(input: IBooking.IBookingPayment): CBookingPayment {
            super.deserialize(input);
            this.open_payment_amount = input.open_payment_amount;
            this.payments = [];
            if (input.payments && input.payments.length) {
                input.payments.forEach((payment: IPayment) => {
                    this.payments.push(new CPayment().deserialize(payment));
                });
            }
            return this;
        }
    }

    export class CBookingLastModifiedDetails extends CAbstractClass
        implements IBooking.IBookingLastModifiedDetails, IDeserializable<CBookingLastModifiedDetails> {
        user_name: string;
        user_email: string;
        last_payment_date: Date;
        last_booking_property_date: Date;

        deserialize(input: IBooking.IBookingLastModifiedDetails): CBookingLastModifiedDetails {
            super.deserialize(input);
            this.user_name = input.user_name;
            this.user_email = input.user_email;
            this.last_payment_date = input.last_payment_date;
            this.last_booking_property_date = input.last_booking_property_date;
            return this;
        }
    }

    export class CBookingNote extends CAbstractClass implements CBooking.CBookingNote, IDeserializable<CBookingNote> {
        content: string;

        deserialize(input: IBooking.IBookingNote): CBookingNote {
            super.deserialize(input);
            this.content = input.content;
            return this;
        }
    }

    export class CBookingProperty extends CAbstractClass implements IBooking.IBookingProperty, IDeserializable<CBookingProperty> {
        value: any;
        code: string;
        type: number;
        title: string;
        last_modified: Date;

        deserialize(input: IBooking.IBookingProperty): CBookingProperty {
            super.deserialize(input);
            this.value = input.value;
            this.code = input.code;
            this.type = input.type;
            this.title = input.title;
            this.last_modified = input.last_modified;
            return this;
        }

        toString() {
            switch (this.type) {
                case 1:
                    return (this.value) ? 'Ja' : 'Nein';
                case 4:
                    return (this.value ) ? new Date(this.value).toLocaleDateString('DE-de', {
                        year: 'numeric',
                        month: '2-digit',
                        day: '2-digit',
                    }) : '';
                default:
                    return this.value;
            }
        }
    }

}
