import CAbstractClass from '../abstract.class';
import {IReminder} from '../../interfaces/twbooking/reminder.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CCustomer} from './customer.class';

export class CReminder extends CAbstractClass implements IReminder, IDeserializable<CReminder> {
    comment: string;
    customer_id: string;
    date: Date;
    reminder_type: IReminder.Type;
    user: {
        _id: string;
        name: string;
        email: string;
    };
    email_send: string;
    customer?: CCustomer;

    deserialize(input: IReminder) {
        super.deserialize(input);
        this.comment = input.comment;
        this.customer_id = input.customer_id;
        this.date = input.date;
        this.reminder_type = input.reminder_type;
        this.user = input.user;
        this.email_send = input.email_send;
        if (input.customer) {
            this.customer = new CCustomer().deserialize(input.customer);
        }
        return this;
    }
}
