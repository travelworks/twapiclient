import {IPayment} from '../../interfaces/twbooking/payment.interface';
import CAbstractClass from '../abstract.class';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class CPayment extends CAbstractClass implements IPayment, IDeserializable<CPayment> {
    payment_id: string;
    email_id: string;
    email_send: boolean;
    open_payment_amount: number;
    payment_amount: number;
    payment_date: Date;

    deserialize(input: IPayment) {
        super.deserialize(input);
        this.payment_id = input.payment_id;
        this.open_payment_amount = input.open_payment_amount;
        this.payment_amount = input.payment_amount;
        this.payment_date = input.payment_date;
        this.email_send = input.email_send;
        this.email_id = input.email_id;
        return this;
    }
}
