import CAbstractClass from '../abstract.class';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {INotificationRule} from '../../interfaces/twbooking/notification.rule.interface';

export class CNotificationRule extends CAbstractClass implements INotificationRule, IDeserializable<CNotificationRule> {
    conditions: INotificationRule.ICondition[];
    email_template: string;
    name: string;
    active: boolean;

    deserialize(input: INotificationRule): CNotificationRule {
        super.deserialize(input);
        this.conditions = input.conditions;
        this.email_template = input.email_template;
        this.name = input.name;
        this.active = input.active;
        return this;
    }
}
