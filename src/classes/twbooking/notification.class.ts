import {INotification} from '../../interfaces/twbooking/notification.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {CAbstractTimestampClass} from '../abstract.timestamp.class';


export class CNotification extends CAbstractTimestampClass implements INotification, IDeserializable<CNotification> {
    email: string;
    notification_rule: string;
    customer: string;
    booking: string;
    payment: string;
    payment_id: string;
    payment_amount: number;
    payment_date: Date;

    deserialize(input: INotification): CNotification {
        super.deserialize(input);
        this.email = input.email;
        this.notification_rule = input.notification_rule;
        this.customer = input.customer;
        this.booking = input.booking;
        this.payment = input.payment;
        this.payment_id = input.payment_id;
        this.payment_amount = input.payment_amount;
        this.payment_date = input.payment_date;
        return this;
    }
}
