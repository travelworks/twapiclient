import CAbstractClass from '../abstract.class';
import {IPaymentReminder} from '../../interfaces/twbooking/paymentReminder.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class CPaymentReminder extends CAbstractClass implements IPaymentReminder, IDeserializable<CPaymentReminder> {
    payment_id: string;
    email_id: string;
    email_send: boolean;
    payment_amount: number;
    reminder_type: string;
    reminder_value: string;

    deserialize(input: IPaymentReminder): CPaymentReminder {
        super.deserialize(input);
        this.payment_id = input.payment_id;
        this.reminder_type = input.reminder_type;
        this.reminder_value = input.reminder_value;
        this.email_send = input.email_send;
        this.email_id = input.email_id;
        this.payment_amount = input.payment_amount;
        return this;
    }
}
