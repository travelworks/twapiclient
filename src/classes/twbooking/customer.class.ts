import CAbstractClass from '../abstract.class';
import {IBooking, ICustomer, IPointAccount} from '../../index';
import {CBooking} from './booking.class';
import {IDeserializable} from '../../interfaces/deserializable.interface';

export class CCustomer extends CAbstractClass implements ICustomer, IDeserializable<CCustomer> {
    address: CCustomer.CAddress = new CCustomer.CAddress();
    bookings: CBooking[];
    customer_last_modified: Date;
    customer_number?: string;
    dob: Date;
    email: string;
    additional_emails: CCustomer.CEmail[];
    first_name: string;
    gender: ICustomer.Gender;
    invoice_email: string;
    last_name: string;
    mobile: string;
    phone: string;
    sub_type: ICustomer.SubType;
    point_status: IPointAccount.Status;
    com_channels: CCustomer.CAdditionalAttribute[];
    customer_properties: CCustomer.CAdditionalAttribute[];
    external_references: CCustomer.CAdditionalAttribute[];

    deserialize(input: ICustomer): CCustomer {
        super.deserialize(input);
        this.gender = input.gender;
        this.last_name = input.last_name;
        this.first_name = input.first_name;
        this.phone = input.phone;
        this.dob = input.dob;
        this.email = input.email;

        if (input.customer_number) {
            this.customer_number = input.customer_number;
        }
        if (input.address) {
            this.address = new CCustomer.CAddress().deserialize(input.address)
        } else {
            this.address = new CCustomer.CAddress();
        }
        this.bookings = [];
        if (input.bookings && input.bookings.length) {
            input.bookings.forEach((booking: IBooking) => {
                this.bookings.push(new CBooking().deserialize(booking));
            });

        }
        this.point_status = input.point_status;
        this.com_channels = [];
        if (input.com_channels && input.com_channels.length) {
            input.com_channels.forEach((entry: ICustomer.IAdditionalAttribute) => {
                this.com_channels.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.customer_properties = [];
        if (input.customer_properties && input.customer_properties.length) {
            input.customer_properties.forEach((entry: ICustomer.IAdditionalAttribute) => {
                this.customer_properties.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.external_references = [];
        if (input.external_references && input.external_references.length) {
            input.external_references.forEach((entry: ICustomer.IAdditionalAttribute) => {
                this.external_references.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.additional_emails = [];
        if (input.additional_emails && input.additional_emails.length) {
            input.additional_emails.forEach((entry: ICustomer.IEmail) => {
                this.additional_emails.push(new CCustomer.CEmail().deserialize(entry));
            });
        }
        return this;
    }

    getAge(): number {
        const ageDifMs = Date.now() - new Date(this.dob).getTime();
        const ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }

}

export namespace CCustomer {
    export class CAddress implements ICustomer.IAddress, IDeserializable<CAddress> {
        additional: string;
        city: string;
        country: string;
        street: string;
        zip: string;

        deserialize(input: any): CAddress {
            this.additional = input.additional;
            this.city = input.city;
            this.country = input.country;
            this.street = input.street;
            this.zip = input.zip;
            return this;
        }
    }

    export class CEmail implements ICustomer.IEmail, IDeserializable<CEmail> {
        email: string;
        description: string;

        deserialize(input: ICustomer.IEmail): CEmail {
            this.email = input.email;
            this.description = input.description;
            return this;
        }


    }

    export class CAdditionalAttribute extends CAbstractClass
        implements ICustomer.IAdditionalAttribute, IDeserializable<CAdditionalAttribute> {
        code: string;
        title: string;
        valid_from: Date;
        valid_to: Date;
        value: string;

        deserialize(input: ICustomer.IAdditionalAttribute): CCustomer.CAdditionalAttribute {
            super.deserialize(input);
            this.code = input.code;
            this.title = input.title;
            this.valid_from = input.valid_from;
            this.valid_to = input.valid_to;
            this.value = input.value;
            return this;
        }

    }

    export class CNote extends CAbstractClass implements ICustomer.INote, IDeserializable<CNote> {
        customer: string;
        contact_method: ICustomer.INote.ContactType;
        contact_person: string;
        content: string;
        createdAt: Date;
        updatedAt: Date;
        type: ICustomer.INote.Type;
        probability: ICustomer.INote.Probability;

        deserialize(input: ICustomer.INote): CCustomer.CNote {
            super.deserialize(input);
            this.customer = input.customer;
            this.contact_method = input.contact_method;
            this.contact_person = input.contact_person;
            this.content = input.content;
            this.createdAt = input.createdAt;
            this.updatedAt = input.updatedAt;
            this.type = input.type;
            this.probability = input.probability;
            return this;
        }

    }
}
