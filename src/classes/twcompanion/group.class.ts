import { IGroup } from "../../interfaces/twcompanion/group.interface";
import { IDeserializable } from "../../interfaces/deserializable.interface";
import CAbstractClass from "../abstract.class";

export class CGroup extends CAbstractClass implements IGroup, IDeserializable<CGroup> {
    _id: string;
    name: string;
    countries: string[] = [];
    products: string[] = [];
    show_map: boolean;

    deserialize(input: any): CGroup {
        this._id = input._id;
        this.name = input.name;
        this.countries = [];
        if (input.countries && input.countries.length) {
            input.countries.forEach((country: any) => {
                this.countries.push(country);
            });
        }
        this.products = [];
        if (input.products && input.products.length) {
            input.products.forEach((product: any) => {
                this.products.push(product);
            });
        }
        this.show_map = input.show_map;
        return this;
    }
}