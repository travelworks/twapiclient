import { IDeserializable } from "../../interfaces/deserializable.interface";
import { ISharedField } from "../../interfaces/twcompanion/sharedfield.interface";

export class CSharedFieldConfig implements ISharedField.IConfig, IDeserializable<CSharedFieldConfig> {
  name: string;
  shared: boolean;
  import_function: boolean;
  title: any;

  deserialize(input: any): CSharedFieldConfig {
    this.name = input.name;
    this.shared = input.shared;
    this.import_function = input.import_function;
    this.title = input.title;
    return this;
  }
}

export class CSharedField implements ISharedField.IField, IDeserializable<CSharedField> {
  _id: string;
  name: string;
  shared: boolean;
  value: any;
  config: CSharedFieldConfig;
  timestamp: Date;
  _previousShared: boolean;

  deserialize(input: any): CSharedField {
    this._id = input._id;
    this.name = input.name;
    this.shared = input.shared;
    this.value = (input.value) ? input.value : null;
    return this;
  }
}