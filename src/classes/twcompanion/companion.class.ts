import {ITrip} from '../../interfaces/twcompanion/trip.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import {ICompanion} from '../../interfaces/twcompanion/companion.interface';
import {ISharedField} from '../../interfaces/twcompanion/sharedfield.interface';
import {ILog} from '../../interfaces/twcompanion/log.interface';
import CAbstractClass from '../abstract.class';
import {CTrip} from './trip.class';
import {CSharedField} from './sharedfield.class';
import {CLog} from './log.class';

export class CCompanion extends CAbstractClass implements ICompanion, IDeserializable<CCompanion> {
    _id: string;
    customer: string;
    trips: ITrip[];
    hash: string;
    shared_fields: ISharedField.IField[];
    log: ILog[];
    mails_send: any[];
    invite_send_date: Date;
    reminder_send_date: Date;
    last_login: Date;

    deserialize(input: ICompanion): CCompanion {
        super.deserialize(input);
        this.customer = input.customer;
        this.trips = [];
        if (input.trips && input.trips.length) {
            input.trips.forEach((itrip: ITrip) => {
                this.trips.push(new CTrip().deserialize(itrip));
            });
        }
        this.hash = input.hash;
        this.shared_fields = [];
        if (input.shared_fields && input.shared_fields.length) {
            input.shared_fields.forEach((isf: ISharedField.IField) => {
                this.shared_fields.push(new CSharedField().deserialize(isf));
            });
        }
        this.log = [];
        if (input.log && input.log.length) {
            input.log.forEach((log: ILog) => {
                this.log.push(new CLog().deserialize(log));
            });
        }
        this.mails_send = input.mails_send;
        this.invite_send_date = input.invite_send_date;
        this.reminder_send_date = input.reminder_send_date;
        this.last_login = input.last_login;
        return this;
    }

}
