import {IDeserializable} from '../../interfaces/deserializable.interface';
import {ILog} from '../../interfaces/twcompanion/log.interface';

export class CLog implements IDeserializable<CLog>, ILog {
    _id: string;
    shared_field_name: string;
    old_value: string;
    new_value: string;
    timestamp: Date;

    deserialize(input: any): CLog {
        this._id = input._id;
        this.shared_field_name = input.shared_field_name;
        this.old_value = input.old_value;
        this.new_value = input.new_value;
        this.timestamp = input.timestamp;
        return this;
    }
}
