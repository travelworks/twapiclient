import { IDeserializable } from "../../interfaces/deserializable.interface";
import { ITrip } from "../../interfaces/twcompanion/trip.interface";
import { IBooking } from "../../interfaces/twbooking/booking.interface";

export class CTrip implements ITrip, IDeserializable<CTrip> {
    _id: string;
    booking: string;
    product_code: string;
    program_code: string;
    program_country: string;
    program_city: IBooking.IProgramCity;
    program_end: Date;
    program_start: Date;

    deserialize(input: any): CTrip {
        this._id = input._id;
        this.product_code = input.product_code;
        this.program_code = input.program_code;
        this.program_country = input.program_country;
        this.program_city = input.program_city;
        this.program_end = input.program_end;
        this.program_start = input.program_start;
        return this;
    }
}