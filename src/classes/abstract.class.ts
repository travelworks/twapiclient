import IAbstractInterface from '../interfaces/abstract.interface';
import {CRequest, IRequest} from '..';

export default class CAbstractClass implements IAbstractInterface {
    _id?: any;

    static deserialize(inputs: any[]) {
        const result: any = [];
        for (const input of inputs) {
            const object: CAbstractClass = new this;
            result.push(object.deserialize(input));
        }
        return result;
    }

    deserialize(input: IAbstractInterface): any {
        this._id = input._id;
        return this;
    }

}
