import CAbstractClass from '../abstract.class';
import {IPointActivity} from '../../interfaces/twpoint/point.activity.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';


export class CPointActivity extends CAbstractClass implements IPointActivity, IDeserializable<CPointActivity> {
    amount: number;
    name: string;

    deserialize(input: IPointActivity): CPointActivity {
        super.deserialize(input);
        this.amount = input.amount;
        this.name = input.name;
        return this;
    }
}
