import CAbstractClass from '../abstract.class';
import {IPointAccount} from '../../interfaces/twpoint/point.account.interface';
import {IDeserializable} from '../../interfaces/deserializable.interface';
import IAbstractInterface from '../../interfaces/abstract.interface';


export class CPointAccount extends CAbstractClass implements IPointAccount, IDeserializable<CPointAccount> {
    createdAt: Date;
    customer: string;
    entries: CPointAccount.CPointEntry[];
    hash: string;
    updatedAt: Date;
    status: IPointAccount.Status;
    status_comment: string;
    current_balance: number;
    notifications: CPointAccount.CPointNotification[];

    deserialize(input: IPointAccount): CPointAccount {
        super.deserialize(input);
        this.createdAt = input.createdAt;
        this.customer = input.customer;
        this.hash = input.hash;
        this.updatedAt = input.updatedAt;
        this.status = input.status;
        this.status_comment = input.status_comment;
        this.entries = [];
        if (input.entries && input.entries.length) {
            this.entries = CPointAccount.CPointEntry.deserialize(input.entries);
        }
        this.notifications = [];
        if (input.notifications && input.notifications.length) {
            this.notifications = CPointAccount.CPointNotification.deserialize(input.notifications);
        }
        this.current_balance = input.current_balance;
        return this;
    }
}

export namespace CPointAccount {

    export class CPointNotification extends CAbstractClass {
        type: IPointAccount.IPointNotification.Type;
        email: string;
        createdAt: Date;
        updatedAt: Date;

        deserialize(input: IPointAccount.IPointNotification): CPointNotification {
            super.deserialize(input);
            this.type = input.type;
            this.email = input.email;
            this.createdAt = input.createdAt;
            this.updatedAt = input.updatedAt;
            return this;
        }
    }

    export class CPointEntry extends CAbstractClass implements IPointAccount.IPointEntry, IDeserializable<CPointEntry> {
        amount: number;
        booking: string;
        createdAt: Date;
        issued_by: string;
        point_activity: string;
        updatedAt: Date;

        deserialize(input: IPointAccount.IPointEntry): CPointEntry {
            super.deserialize(input);
            this.amount = input.amount;
            this.booking = input.booking;
            this.createdAt = input.createdAt;
            this.issued_by = input.issued_by;
            this.point_activity = input.point_activity;
            this.updatedAt = input.updatedAt;
            return this;
        }
    }
}
