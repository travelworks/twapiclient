import CAbstractClass from './abstract.class';
import IAbstractInterface from '../interfaces/abstract.interface';
import IAbstractTimeStampInterface from '../interfaces/abstract.timestamp.interface';


export abstract class CAbstractTimestampClass extends CAbstractClass implements IAbstractTimeStampInterface  {
    createdAt: Date;
    updatedAt: Date;

    deserialize(input: any): any {
        super.deserialize(input);
        this.createdAt = input.createdAt;
        this.updatedAt = input.updatedAt;
        return this;
    }
}
