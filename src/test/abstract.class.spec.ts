import {COption, IOption} from '..';

const dummyOptions: IOption[] = [
    {
        label: '1',
        value: '1'
    },
    {
        label: '2',
        value: '2'
    },
    {
        label: '3',
        value: '3'
    },

]

const options = COption.deserialize(dummyOptions);
console.log(options);

