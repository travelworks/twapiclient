import {CCustomer, IBooking, ICustomer, IPointAccount, TwBookingClient} from '..';
import {ITwListResult} from '../clients/twAbstractApiClient';

const dummyCustomers: ICustomer[] | any[] = [
    {
        address: undefined,
        bookings: [],
        customer_last_modified: undefined,
        customer_number: '0',
        dob: undefined,
        email: '',
        first_name: '1',
        gender: undefined,
        invoice_email: '',
        last_name: '1',
        mobile: '',
        phone: '',
        sub_type: undefined,
        point_status: IPointAccount.Status.open
    },
    {
        address: undefined,
        bookings: [],
        customer_last_modified: undefined,
        customer_number: '0',
        dob: undefined,
        email: '',
        first_name: '2',
        gender: undefined,
        invoice_email: '',
        last_name: '2',
        mobile: '',
        phone: '',
        sub_type: undefined,
        point_status: IPointAccount.Status.open
    }
];

class DCustomer extends CCustomer {
}

class CustomerClientTest extends TwBookingClient.CustomerClient {
    Class = DCustomer;

    async list(query: any | null = null, sorting: any | null = null, paging: any | null = null, projection: any | null = null)
        : Promise<ITwListResult<CCustomer>> {
        const listResult: ITwListResult<CCustomer> = {
            items: [],
            totalCount: 0
        }
        listResult.items = this.Class.deserialize(dummyCustomers);
        listResult.totalCount = listResult.items.length;
        console.log(listResult);
        return listResult;
    }

}

const customerClientTest = new CustomerClientTest('test');
customerClientTest.list();
