import CAbstractClass from '../abstract.class';
import { IAttachment, IEmailTemplate } from '../../index';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CEmailTemplate extends CAbstractClass implements IEmailTemplate, IDeserializable<CEmailTemplate> {
    attachments: CAttachment[];
    from: string;
    html: string;
    name: string;
    placeholders: IEmailTemplate.IPlaceholder[];
    subject: string;
    text: string;
    type: string;
    deserialize(input: IEmailTemplate): CEmailTemplate;
}
export declare class CAttachment extends CAbstractClass implements IAttachment, IDeserializable<CAttachment> {
    data: any;
    meta: any;
    deserialize(input: any): CAttachment;
}
//# sourceMappingURL=emailTemplate.class.d.ts.map