"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CAttachment = exports.CEmailTemplate = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CEmailTemplate = /** @class */ (function (_super) {
    __extends(CEmailTemplate, _super);
    function CEmailTemplate() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CEmailTemplate.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.attachments = [];
        if (input.attachments && input.attachments.length) {
            input.attachments.forEach(function (attachment) {
                _this.attachments.push(new CAttachment().deserialize(attachment));
            });
        }
        this.from = input.from;
        this.html = input.html;
        this.name = input.name;
        this.placeholders = input.placeholders;
        this.subject = input.subject;
        this.text = input.text;
        this.type = input.type;
        return this;
    };
    return CEmailTemplate;
}(abstract_class_1.default));
exports.CEmailTemplate = CEmailTemplate;
var CAttachment = /** @class */ (function (_super) {
    __extends(CAttachment, _super);
    function CAttachment() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CAttachment.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.data = input.data;
        this.meta = input.meta;
        return this;
    };
    return CAttachment;
}(abstract_class_1.default));
exports.CAttachment = CAttachment;
//# sourceMappingURL=emailTemplate.class.js.map