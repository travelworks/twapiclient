"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CEmail = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var emailTemplate_class_1 = require("./emailTemplate.class");
var CEmail = /** @class */ (function (_super) {
    __extends(CEmail, _super);
    function CEmail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CEmail.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.attachments = [];
        if (input.attachments && input.attachments.length) {
            input.attachments.forEach(function (attachment) {
                _this.attachments.push(new emailTemplate_class_1.CAttachment().deserialize(attachment));
            });
        }
        this.bcc = input.bcc;
        this.cc = input.cc;
        if (input.emailTemplate) {
            this.emailTemplate = new emailTemplate_class_1.CEmailTemplate().deserialize(input.emailTemplate);
        }
        this.from = input.from;
        this.html = input.html;
        this.placeholders = input.placeholders;
        this.send_status = input.send_status;
        this.subject = input.subject;
        this.text = input.text;
        this.to = input.to;
        this.createdAt = input.createdAt;
        return this;
    };
    return CEmail;
}(abstract_class_1.default));
exports.CEmail = CEmail;
//# sourceMappingURL=email.class.js.map