import CAbstractClass from '../abstract.class';
import { IEmail } from '../..';
import { CAttachment, CEmailTemplate } from './emailTemplate.class';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CEmail extends CAbstractClass implements IEmail, IDeserializable<CEmail> {
    attachments: CAttachment[];
    bcc: string[];
    cc: string[];
    emailTemplate: CEmailTemplate;
    from: string;
    html: string;
    placeholders: IEmail.IPlaceholders;
    send_status: IEmail.ISendStatus;
    subject: string;
    text: string;
    to: string[];
    createdAt: Date;
    deserialize(input: IEmail): CEmail;
}
//# sourceMappingURL=email.class.d.ts.map