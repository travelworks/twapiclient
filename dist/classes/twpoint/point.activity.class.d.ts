import CAbstractClass from '../abstract.class';
import { IPointActivity } from '../../interfaces/twpoint/point.activity.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CPointActivity extends CAbstractClass implements IPointActivity, IDeserializable<CPointActivity> {
    amount: number;
    name: string;
    deserialize(input: IPointActivity): CPointActivity;
}
//# sourceMappingURL=point.activity.class.d.ts.map