"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CPointAccount = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CPointAccount = /** @class */ (function (_super) {
    __extends(CPointAccount, _super);
    function CPointAccount() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CPointAccount.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.createdAt = input.createdAt;
        this.customer = input.customer;
        this.hash = input.hash;
        this.updatedAt = input.updatedAt;
        this.status = input.status;
        this.status_comment = input.status_comment;
        this.entries = [];
        if (input.entries && input.entries.length) {
            this.entries = CPointAccount.CPointEntry.deserialize(input.entries);
        }
        this.notifications = [];
        if (input.notifications && input.notifications.length) {
            this.notifications = CPointAccount.CPointNotification.deserialize(input.notifications);
        }
        this.current_balance = input.current_balance;
        return this;
    };
    return CPointAccount;
}(abstract_class_1.default));
exports.CPointAccount = CPointAccount;
(function (CPointAccount) {
    var CPointNotification = /** @class */ (function (_super) {
        __extends(CPointNotification, _super);
        function CPointNotification() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CPointNotification.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.type = input.type;
            this.email = input.email;
            this.createdAt = input.createdAt;
            this.updatedAt = input.updatedAt;
            return this;
        };
        return CPointNotification;
    }(abstract_class_1.default));
    CPointAccount.CPointNotification = CPointNotification;
    var CPointEntry = /** @class */ (function (_super) {
        __extends(CPointEntry, _super);
        function CPointEntry() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CPointEntry.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.amount = input.amount;
            this.booking = input.booking;
            this.createdAt = input.createdAt;
            this.issued_by = input.issued_by;
            this.point_activity = input.point_activity;
            this.updatedAt = input.updatedAt;
            return this;
        };
        return CPointEntry;
    }(abstract_class_1.default));
    CPointAccount.CPointEntry = CPointEntry;
})(CPointAccount = exports.CPointAccount || (exports.CPointAccount = {}));
exports.CPointAccount = CPointAccount;
//# sourceMappingURL=point.account.class.js.map