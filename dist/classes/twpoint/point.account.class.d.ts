import CAbstractClass from '../abstract.class';
import { IPointAccount } from '../../interfaces/twpoint/point.account.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CPointAccount extends CAbstractClass implements IPointAccount, IDeserializable<CPointAccount> {
    createdAt: Date;
    customer: string;
    entries: CPointAccount.CPointEntry[];
    hash: string;
    updatedAt: Date;
    status: IPointAccount.Status;
    status_comment: string;
    current_balance: number;
    notifications: CPointAccount.CPointNotification[];
    deserialize(input: IPointAccount): CPointAccount;
}
export declare namespace CPointAccount {
    class CPointNotification extends CAbstractClass {
        type: IPointAccount.IPointNotification.Type;
        email: string;
        createdAt: Date;
        updatedAt: Date;
        deserialize(input: IPointAccount.IPointNotification): CPointNotification;
    }
    class CPointEntry extends CAbstractClass implements IPointAccount.IPointEntry, IDeserializable<CPointEntry> {
        amount: number;
        booking: string;
        createdAt: Date;
        issued_by: string;
        point_activity: string;
        updatedAt: Date;
        deserialize(input: IPointAccount.IPointEntry): CPointEntry;
    }
}
//# sourceMappingURL=point.account.class.d.ts.map