import CAbstractClass from '../abstract.class';
import { IRequest } from '../../interfaces/twsurvey/request.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CBooking } from '../twbooking/booking.class';
import { CCustomer } from '../twbooking/customer.class';
import { CSurvey } from './survey.class';
export declare class CRequest extends CAbstractClass implements IRequest, IDeserializable<CRequest> {
    booking: any;
    customer: any;
    email_status: CRequest.CEMailStatus;
    hash: string;
    internal: CRequest.CInternal;
    is_new: boolean;
    response: CSurvey;
    status: string;
    survey: any;
    createdAt: Date;
    updatedAt: Date;
    deserialize(input: IRequest): CRequest;
}
export declare namespace CRequest {
    class CEMailStatus implements IRequest.IEMailStatus, IDeserializable<CEMailStatus> {
        firstEmail?: any;
        secondEmail?: any;
        deserialize(input: any): CRequest.CEMailStatus;
    }
    class CInternal implements IRequest.IInternal, IDeserializable<CInternal> {
        customer: CCustomer;
        booking: CBooking;
        deserialize(input: any): CRequest.CInternal;
    }
}
//# sourceMappingURL=request.class.d.ts.map