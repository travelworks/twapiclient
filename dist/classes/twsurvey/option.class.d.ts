import CAbstractClass from '../abstract.class';
import { IOption } from '../../interfaces/twsurvey/option.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class COption extends CAbstractClass implements IOption, IDeserializable<COption> {
    label: string;
    value: string;
    deserialize(input: IOption): COption;
}
//# sourceMappingURL=option.class.d.ts.map