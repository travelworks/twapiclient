"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CQuestion = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var option_class_1 = require("./option.class");
var CQuestion = /** @class */ (function (_super) {
    __extends(CQuestion, _super);
    function CQuestion() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CQuestion.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.conditions = [];
        if (input.conditions && input.conditions.length) {
            input.conditions.forEach(function (condition) {
                _this.conditions.push(new option_class_1.COption().deserialize(condition));
            });
        }
        this.input_type = input.input_type;
        this.keyword = input.keyword;
        this.limit = input.limit;
        this.name = input.name;
        this.options = [];
        if (input.options && input.options.length) {
            input.options.forEach(function (option) {
                _this.options.push(new option_class_1.COption().deserialize(option));
            });
        }
        this.required = input.required;
        this.text = input.text;
        this.type = input.type;
        this.answer = input.answer;
        return this;
    };
    CQuestion.prototype.getAnswer = function (label) {
        if (label === void 0) { label = false; }
        switch (this.type) {
            case 'choice':
                if (label) {
                    return this.answer.label;
                }
                else {
                    return this.answer.value;
                }
            case 'multichoice':
                if (label) {
                    return this.answer.map(function (option) { return option.label; }).join(', ');
                }
                else {
                    return this.answer.map(function (option) { return option.value; }).join(', ');
                }
            case 'rating':
            case 'starrating':
                return ((this.answer / this.limit * 100).toFixed(1) + '%').replace('.', ',');
            default:
                return this.answer;
        }
    };
    CQuestion.prototype.conditionFulfilled = function (operator) {
        var _this = this;
        if (Array.isArray(operator)) {
            return operator.some(function (ov) { return _this.conditions.includes(ov); });
        }
        else {
            return this.conditions.includes(operator);
        }
    };
    return CQuestion;
}(abstract_class_1.default));
exports.CQuestion = CQuestion;
//# sourceMappingURL=question.class.js.map