"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CSurvey = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var question_class_1 = require("./question.class");
var CSurvey = /** @class */ (function (_super) {
    __extends(CSurvey, _super);
    function CSurvey() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CSurvey.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.active = input.active;
        if (input.condition) {
            this.condition = new CSurvey.CCondition().deserialize(input.condition);
        }
        if (input.mailSettings) {
            this.mailSettings = new CSurvey.CMailSettings().deserialize(input.mailSettings);
        }
        this.name = input.name;
        this.questions = [];
        if (input.questions && input.questions.length) {
            input.questions.forEach(function (question) {
                _this.questions.push(new question_class_1.CQuestion().deserialize(question));
            });
        }
        this.responses = [];
        if (input.responses && input.responses.length) {
            input.responses.forEach(function (response) {
                _this.responses.push(new CSurvey().deserialize(response));
            });
        }
        this.surveys = [];
        if (input.surveys && input.surveys.length) {
            input.surveys.forEach(function (survey) {
                _this.surveys.push(new CSurvey().deserialize(survey));
            });
        }
        this.thankYouText = input.thankYouText;
        this.ttl = input.ttl;
        this.type = input.type;
        return this;
    };
    return CSurvey;
}(abstract_class_1.default));
exports.CSurvey = CSurvey;
(function (CSurvey) {
    var CCondition = /** @class */ (function () {
        function CCondition() {
        }
        CCondition.prototype.deserialize = function (input) {
            this.end = input.end;
            this.products = [];
            if (input.products && input.products.length) {
                this.products = input.products;
            }
            this.programs = [];
            if (input.programs && input.programs.length) {
                this.programs = input.programs;
            }
            this.start = input.start;
            return this;
        };
        return CCondition;
    }());
    CSurvey.CCondition = CCondition;
    var CMailSettings = /** @class */ (function () {
        function CMailSettings() {
        }
        CMailSettings.prototype.deserialize = function (input) {
            this.firstTemplate = input.firstTemplate;
            this.remindingDays = input.remindingDays;
            this.secondTemplate = input.secondTemplate;
            return this;
        };
        return CMailSettings;
    }());
    CSurvey.CMailSettings = CMailSettings;
})(CSurvey = exports.CSurvey || (exports.CSurvey = {}));
exports.CSurvey = CSurvey;
//# sourceMappingURL=survey.class.js.map