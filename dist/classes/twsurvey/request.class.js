"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CRequest = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var booking_class_1 = require("../twbooking/booking.class");
var customer_class_1 = require("../twbooking/customer.class");
var survey_class_1 = require("./survey.class");
var CRequest = /** @class */ (function (_super) {
    __extends(CRequest, _super);
    function CRequest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CRequest.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.booking = input.booking;
        this.customer = input.customer;
        if (input.email_status) {
            this.email_status = new CRequest.CEMailStatus().deserialize(input.email_status);
        }
        this.hash = input.hash;
        if (input.internal) {
            this.internal = new CRequest.CInternal().deserialize(input.internal);
        }
        this.is_new = input.is_new;
        if (input.response) {
            this.response = new survey_class_1.CSurvey().deserialize(input.response);
        }
        this.status = input.status;
        this.survey = input.survey;
        this.createdAt = input.createdAt;
        this.updatedAt = input.updatedAt;
        return this;
    };
    return CRequest;
}(abstract_class_1.default));
exports.CRequest = CRequest;
(function (CRequest) {
    var CEMailStatus = /** @class */ (function () {
        function CEMailStatus() {
        }
        CEMailStatus.prototype.deserialize = function (input) {
            this.firstEmail = input.firstEmail;
            this.secondEmail = input.secondEmail;
            return this;
        };
        return CEMailStatus;
    }());
    CRequest.CEMailStatus = CEMailStatus;
    var CInternal = /** @class */ (function () {
        function CInternal() {
        }
        CInternal.prototype.deserialize = function (input) {
            this.customer = new customer_class_1.CCustomer().deserialize(input.customer);
            this.booking = new booking_class_1.CBooking().deserialize(input.booking);
            return this;
        };
        return CInternal;
    }());
    CRequest.CInternal = CInternal;
})(CRequest = exports.CRequest || (exports.CRequest = {}));
exports.CRequest = CRequest;
//# sourceMappingURL=request.class.js.map