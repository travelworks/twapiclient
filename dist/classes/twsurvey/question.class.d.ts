import CAbstractClass from '../abstract.class';
import { IQuestion } from '../../interfaces/twsurvey/question.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { COption } from './option.class';
export declare class CQuestion extends CAbstractClass implements IQuestion, IDeserializable<CQuestion> {
    answer: any;
    conditions: COption[];
    input_type: IQuestion.InputTypes;
    keyword: string;
    limit: number;
    name: string;
    options: COption[];
    required: boolean;
    text: string;
    type: IQuestion.Types;
    questions: CQuestion[];
    deserialize(input: IQuestion): CQuestion;
    getAnswer(label?: boolean): string;
    conditionFulfilled(operator: any): Boolean;
}
//# sourceMappingURL=question.class.d.ts.map