"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.COption = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var COption = /** @class */ (function (_super) {
    __extends(COption, _super);
    function COption() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    COption.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.label = input.label;
        this.value = input.value;
        return this;
    };
    return COption;
}(abstract_class_1.default));
exports.COption = COption;
//# sourceMappingURL=option.class.js.map