import CAbstractClass from '../abstract.class';
import { ISurvey } from '../../interfaces/twsurvey/survey.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CQuestion } from './question.class';
export declare class CSurvey extends CAbstractClass implements ISurvey, IDeserializable<CSurvey> {
    active: boolean;
    condition: CSurvey.CCondition;
    mailSettings: CSurvey.CMailSettings;
    name: string;
    questions: CQuestion[];
    responses: CSurvey[];
    surveys: any;
    thankYouText: string;
    ttl: number;
    type: string;
    deserialize(input: ISurvey): CSurvey;
}
export declare namespace CSurvey {
    class CCondition implements ISurvey.ICondition, IDeserializable<CCondition> {
        end: number;
        products: string[];
        programs: string[];
        start: number;
        deserialize(input: any): CCondition;
    }
    class CMailSettings implements ISurvey.IMailSettings, IDeserializable<CMailSettings> {
        firstTemplate: string;
        remindingDays: number;
        secondTemplate: string;
        deserialize(input: any): CSurvey.CMailSettings;
    }
}
//# sourceMappingURL=survey.class.d.ts.map