import CAbstractClass from './abstract.class';
import IAbstractTimeStampInterface from '../interfaces/abstract.timestamp.interface';
export declare abstract class CAbstractTimestampClass extends CAbstractClass implements IAbstractTimeStampInterface {
    createdAt: Date;
    updatedAt: Date;
    deserialize(input: any): any;
}
//# sourceMappingURL=abstract.timestamp.class.d.ts.map