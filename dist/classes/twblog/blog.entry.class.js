"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CBlogEntry = void 0;
var abstract_timestamp_class_1 = require("../abstract.timestamp.class");
var blog_attachment_class_1 = require("./blog.attachment.class");
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CBlogEntry = /** @class */ (function (_super) {
    __extends(CBlogEntry, _super);
    function CBlogEntry() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.imported = false;
        return _this;
    }
    CBlogEntry.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.blog_account = input.blog_account;
        this.body = input.body;
        this.booking = new CBlogEntry.CBlogBooking().deserialize(input.booking);
        this.customer = input.customer;
        this.comment = input.comment;
        this.deletion_request = input.deletion_request;
        this.exploitation_right_confirmed = input.exploitation_right_confirmed;
        this.media = input.media;
        this.new = input.new;
        this.originator_confirmed = input.originator_confirmed;
        this.public = input.public;
        this.title = input.title;
        this.published = input.published;
        if (input.teaser_attachment) {
            this.teaser_attachment = new blog_attachment_class_1.CBlogAttachment().deserialize(input.teaser_attachment);
        }
        this.imported = !!input.imported;
        return this;
    };
    return CBlogEntry;
}(abstract_timestamp_class_1.CAbstractTimestampClass));
exports.CBlogEntry = CBlogEntry;
(function (CBlogEntry) {
    var CBlogBooking = /** @class */ (function (_super) {
        __extends(CBlogBooking, _super);
        function CBlogBooking() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CBlogBooking.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.booking_number = input.booking_number;
            this.program_code = input.program_code;
            this.product_code = input.product_code;
            this.program_country = input.program_country;
            this.program_end = input.program_end;
            this.program_name = input.program_name;
            this.program_start = input.program_start;
            return this;
        };
        return CBlogBooking;
    }(abstract_class_1.default));
    CBlogEntry.CBlogBooking = CBlogBooking;
})(CBlogEntry = exports.CBlogEntry || (exports.CBlogEntry = {}));
exports.CBlogEntry = CBlogEntry;
//# sourceMappingURL=blog.entry.class.js.map