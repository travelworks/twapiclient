import { CAbstractTimestampClass } from '../abstract.timestamp.class';
import { IBlogAccount } from '../../interfaces/twblog/blog.account.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CBlogAccount extends CAbstractTimestampClass implements IBlogAccount, IDeserializable<CBlogAccount> {
    customer: IBlogAccount.IBlogCustomer;
    hash: string;
    notifications: IBlogAccount.IBlogNotification[];
    user_name: string;
    deserialize(input: IBlogAccount): any;
}
//# sourceMappingURL=blog.account.class.d.ts.map