import { CAbstractTimestampClass } from '../abstract.timestamp.class';
import { IBlogEntry } from '../../interfaces/twblog/blog.entry.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CBlogAttachment } from './blog.attachment.class';
import { IBlogAccount } from '../../interfaces/twblog/blog.account.interface';
import CAbstractClass from '../abstract.class';
export declare class CBlogEntry extends CAbstractTimestampClass implements IBlogEntry, IDeserializable<CBlogEntry> {
    blog_account: string;
    body: string;
    booking: IBlogEntry.IBlogBooking;
    customer: IBlogAccount.IBlogCustomer;
    comment: string;
    deletion_request: Date;
    exploitation_right_confirmed: boolean;
    media: string[];
    new: boolean;
    originator_confirmed: boolean;
    public: boolean;
    title: string;
    published: boolean;
    teaser_attachment: CBlogAttachment;
    imported: boolean;
    deserialize(input: IBlogEntry): any;
}
export declare namespace CBlogEntry {
    class CBlogBooking extends CAbstractClass implements IBlogEntry.IBlogBooking, IDeserializable<CBlogBooking> {
        booking_number: string;
        product_code: string;
        program_code: string;
        program_country: string;
        program_end: Date;
        program_name: string;
        program_start: Date;
        deserialize(input: IBlogEntry.IBlogBooking): any;
    }
}
//# sourceMappingURL=blog.entry.class.d.ts.map