import { CAbstractTimestampClass } from '../abstract.timestamp.class';
import { IBlogAttachment } from '../../interfaces/twblog/blog.attachment.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { IBlogEntry } from '../../interfaces/twblog/blog.entry.interface';
import { IBlogAccount } from '../../interfaces/twblog/blog.account.interface';
export declare class CBlogAttachment extends CAbstractTimestampClass implements IBlogAttachment, IDeserializable<CBlogAttachment> {
    link: string;
    meta: any;
    blog_account: string;
    booking: IBlogEntry.IBlogBooking;
    customer: IBlogAccount.IBlogCustomer;
    eTag: string;
    put_url: string;
    deserialize(input: IBlogAttachment): any;
}
//# sourceMappingURL=blog.attachment.class.d.ts.map