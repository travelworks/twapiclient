"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CBlogAttachment = void 0;
var abstract_timestamp_class_1 = require("../abstract.timestamp.class");
var blog_entry_class_1 = require("./blog.entry.class");
var CBlogAttachment = /** @class */ (function (_super) {
    __extends(CBlogAttachment, _super);
    function CBlogAttachment() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CBlogAttachment.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.link = input.link;
        this.meta = input.meta;
        this.blog_account = input.blog_account;
        this.booking = new blog_entry_class_1.CBlogEntry.CBlogBooking().deserialize(input.booking);
        this.customer = input.customer;
        this.eTag = input.eTag;
        this.put_url = input.put_url;
        return this;
    };
    return CBlogAttachment;
}(abstract_timestamp_class_1.CAbstractTimestampClass));
exports.CBlogAttachment = CBlogAttachment;
//# sourceMappingURL=blog.attachment.class.js.map