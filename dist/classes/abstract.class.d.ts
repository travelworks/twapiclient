import IAbstractInterface from '../interfaces/abstract.interface';
export default class CAbstractClass implements IAbstractInterface {
    _id?: any;
    static deserialize(inputs: any[]): any;
    deserialize(input: IAbstractInterface): any;
}
//# sourceMappingURL=abstract.class.d.ts.map