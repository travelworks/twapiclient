"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CBooking = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var paymentReminder_class_1 = require("./paymentReminder.class");
var payment_class_1 = require("./payment.class");
var customer_class_1 = require("./customer.class");
var CBooking = /** @class */ (function (_super) {
    __extends(CBooking, _super);
    function CBooking() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CBooking.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.booking_number = input.booking_number;
        this.last_modified = input.last_modified;
        this.booking_status = input.booking_status;
        this.payment_status = input.payment_status;
        this.product_code = input.product_code;
        this.web_code = input.web_code;
        this.program_code = input.program_code;
        this.program_name = input.program_name;
        this.program_start = input.program_start;
        this.program_end = input.program_end;
        this.program_country = input.program_country;
        this.program_duration = input.program_duration;
        this.payments = [];
        if (input.payments && input.payments.length) {
            input.payments.forEach(function (payment) {
                _this.payments.push(new payment_class_1.CPayment().deserialize(payment));
            });
        }
        this.payment_reminders = [];
        if (input.payment_reminders && input.payment_reminders.length) {
            input.payment_reminders.forEach(function (payment_reminder) {
                _this.payment_reminders.push(new paymentReminder_class_1.CPaymentReminder().deserialize(payment_reminder));
            });
        }
        if (input.program_city) {
            this.program_city = new CBooking.CProgramCity().deserialize(input.program_city);
        }
        this.insurance_type = input.insurance_type;
        this.insurance_start = input.insurance_start;
        this.insurance_end = input.insurance_end;
        this.travel_begin = input.travel_begin;
        if (input.customer && typeof input.customer === 'object') {
            this.customer = new customer_class_1.CCustomer().deserialize(input.customer);
        }
        if (input.payment) {
            this.payment = new CBooking.CBookingPayment().deserialize(input.payment);
        }
        this.booking_properties = [];
        if (input.booking_properties && input.booking_properties.length) {
            input.booking_properties.forEach(function (booking_property) {
                _this.booking_properties.push(new CBooking.CBookingProperty().deserialize(booking_property));
            });
        }
        this.booking_notes = [];
        if (input.booking_notes && input.booking_notes.length) {
            input.booking_notes.forEach(function (booking_note) {
                _this.booking_notes.push(new CBooking.CBookingNote().deserialize(booking_note));
            });
        }
        if (input.last_modified_details) {
            this.last_modified_details = new CBooking.CBookingLastModifiedDetails().deserialize(input.last_modified_details);
        }
        return this;
    };
    return CBooking;
}(abstract_class_1.default));
exports.CBooking = CBooking;
(function (CBooking) {
    var CProgramCity = /** @class */ (function (_super) {
        __extends(CProgramCity, _super);
        function CProgramCity() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CProgramCity.prototype.deserialize = function (input) {
            this.lat = input.lat;
            this.lng = input.lng;
            this.name = input.name;
            return this;
        };
        return CProgramCity;
    }(abstract_class_1.default));
    CBooking.CProgramCity = CProgramCity;
    var CBookingPayment = /** @class */ (function (_super) {
        __extends(CBookingPayment, _super);
        function CBookingPayment() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CBookingPayment.prototype.deserialize = function (input) {
            var _this = this;
            _super.prototype.deserialize.call(this, input);
            this.open_payment_amount = input.open_payment_amount;
            this.payments = [];
            if (input.payments && input.payments.length) {
                input.payments.forEach(function (payment) {
                    _this.payments.push(new payment_class_1.CPayment().deserialize(payment));
                });
            }
            return this;
        };
        return CBookingPayment;
    }(abstract_class_1.default));
    CBooking.CBookingPayment = CBookingPayment;
    var CBookingLastModifiedDetails = /** @class */ (function (_super) {
        __extends(CBookingLastModifiedDetails, _super);
        function CBookingLastModifiedDetails() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CBookingLastModifiedDetails.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.user_name = input.user_name;
            this.user_email = input.user_email;
            this.last_payment_date = input.last_payment_date;
            this.last_booking_property_date = input.last_booking_property_date;
            return this;
        };
        return CBookingLastModifiedDetails;
    }(abstract_class_1.default));
    CBooking.CBookingLastModifiedDetails = CBookingLastModifiedDetails;
    var CBookingNote = /** @class */ (function (_super) {
        __extends(CBookingNote, _super);
        function CBookingNote() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CBookingNote.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.content = input.content;
            return this;
        };
        return CBookingNote;
    }(abstract_class_1.default));
    CBooking.CBookingNote = CBookingNote;
    var CBookingProperty = /** @class */ (function (_super) {
        __extends(CBookingProperty, _super);
        function CBookingProperty() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CBookingProperty.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.value = input.value;
            this.code = input.code;
            this.type = input.type;
            this.title = input.title;
            this.last_modified = input.last_modified;
            return this;
        };
        CBookingProperty.prototype.toString = function () {
            switch (this.type) {
                case 1:
                    return (this.value) ? 'Ja' : 'Nein';
                case 4:
                    return (this.value) ? new Date(this.value).toLocaleDateString('DE-de', {
                        year: 'numeric',
                        month: '2-digit',
                        day: '2-digit',
                    }) : '';
                default:
                    return this.value;
            }
        };
        return CBookingProperty;
    }(abstract_class_1.default));
    CBooking.CBookingProperty = CBookingProperty;
})(CBooking = exports.CBooking || (exports.CBooking = {}));
exports.CBooking = CBooking;
//# sourceMappingURL=booking.class.js.map