import CAbstractClass from '../abstract.class';
import { IPaymentReminder } from '../../interfaces/twbooking/paymentReminder.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CPaymentReminder extends CAbstractClass implements IPaymentReminder, IDeserializable<CPaymentReminder> {
    payment_id: string;
    email_id: string;
    email_send: boolean;
    payment_amount: number;
    reminder_type: string;
    reminder_value: string;
    deserialize(input: IPaymentReminder): CPaymentReminder;
}
//# sourceMappingURL=paymentReminder.class.d.ts.map