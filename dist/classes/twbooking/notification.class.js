"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CNotification = void 0;
var abstract_timestamp_class_1 = require("../abstract.timestamp.class");
var CNotification = /** @class */ (function (_super) {
    __extends(CNotification, _super);
    function CNotification() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CNotification.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.email = input.email;
        this.notification_rule = input.notification_rule;
        this.customer = input.customer;
        this.booking = input.booking;
        this.payment = input.payment;
        this.payment_id = input.payment_id;
        this.payment_amount = input.payment_amount;
        this.payment_date = input.payment_date;
        return this;
    };
    return CNotification;
}(abstract_timestamp_class_1.CAbstractTimestampClass));
exports.CNotification = CNotification;
//# sourceMappingURL=notification.class.js.map