import CAbstractClass from '../abstract.class';
import { IReminder } from '../../interfaces/twbooking/reminder.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CCustomer } from './customer.class';
export declare class CReminder extends CAbstractClass implements IReminder, IDeserializable<CReminder> {
    comment: string;
    customer_id: string;
    date: Date;
    reminder_type: IReminder.Type;
    user: {
        _id: string;
        name: string;
        email: string;
    };
    email_send: string;
    customer?: CCustomer;
    deserialize(input: IReminder): this;
}
//# sourceMappingURL=reminder.class.d.ts.map