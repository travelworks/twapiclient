import CAbstractClass from '../abstract.class';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { INotificationRule } from '../../interfaces/twbooking/notification.rule.interface';
export declare class CNotificationRule extends CAbstractClass implements INotificationRule, IDeserializable<CNotificationRule> {
    conditions: INotificationRule.ICondition[];
    email_template: string;
    name: string;
    active: boolean;
    deserialize(input: INotificationRule): CNotificationRule;
}
//# sourceMappingURL=notification.rule.class.d.ts.map