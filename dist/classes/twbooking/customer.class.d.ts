import CAbstractClass from '../abstract.class';
import { ICustomer, IPointAccount } from '../../index';
import { CBooking } from './booking.class';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CCustomer extends CAbstractClass implements ICustomer, IDeserializable<CCustomer> {
    address: CCustomer.CAddress;
    bookings: CBooking[];
    customer_last_modified: Date;
    customer_number?: string;
    dob: Date;
    email: string;
    additional_emails: CCustomer.CEmail[];
    first_name: string;
    gender: ICustomer.Gender;
    invoice_email: string;
    last_name: string;
    mobile: string;
    phone: string;
    sub_type: ICustomer.SubType;
    point_status: IPointAccount.Status;
    com_channels: CCustomer.CAdditionalAttribute[];
    customer_properties: CCustomer.CAdditionalAttribute[];
    external_references: CCustomer.CAdditionalAttribute[];
    deserialize(input: ICustomer): CCustomer;
    getAge(): number;
}
export declare namespace CCustomer {
    class CAddress implements ICustomer.IAddress, IDeserializable<CAddress> {
        additional: string;
        city: string;
        country: string;
        street: string;
        zip: string;
        deserialize(input: any): CAddress;
    }
    class CEmail implements ICustomer.IEmail, IDeserializable<CEmail> {
        email: string;
        description: string;
        deserialize(input: ICustomer.IEmail): CEmail;
    }
    class CAdditionalAttribute extends CAbstractClass implements ICustomer.IAdditionalAttribute, IDeserializable<CAdditionalAttribute> {
        code: string;
        title: string;
        valid_from: Date;
        valid_to: Date;
        value: string;
        deserialize(input: ICustomer.IAdditionalAttribute): CCustomer.CAdditionalAttribute;
    }
    class CNote extends CAbstractClass implements ICustomer.INote, IDeserializable<CNote> {
        customer: string;
        contact_method: ICustomer.INote.ContactType;
        contact_person: string;
        content: string;
        createdAt: Date;
        updatedAt: Date;
        type: ICustomer.INote.Type;
        probability: ICustomer.INote.Probability;
        deserialize(input: ICustomer.INote): CCustomer.CNote;
    }
}
//# sourceMappingURL=customer.class.d.ts.map