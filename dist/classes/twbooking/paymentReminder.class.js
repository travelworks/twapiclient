"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CPaymentReminder = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CPaymentReminder = /** @class */ (function (_super) {
    __extends(CPaymentReminder, _super);
    function CPaymentReminder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CPaymentReminder.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.payment_id = input.payment_id;
        this.reminder_type = input.reminder_type;
        this.reminder_value = input.reminder_value;
        this.email_send = input.email_send;
        this.email_id = input.email_id;
        this.payment_amount = input.payment_amount;
        return this;
    };
    return CPaymentReminder;
}(abstract_class_1.default));
exports.CPaymentReminder = CPaymentReminder;
//# sourceMappingURL=paymentReminder.class.js.map