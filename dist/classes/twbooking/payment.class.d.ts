import { IPayment } from '../../interfaces/twbooking/payment.interface';
import CAbstractClass from '../abstract.class';
import { IDeserializable } from '../../interfaces/deserializable.interface';
export declare class CPayment extends CAbstractClass implements IPayment, IDeserializable<CPayment> {
    payment_id: string;
    email_id: string;
    email_send: boolean;
    open_payment_amount: number;
    payment_amount: number;
    payment_date: Date;
    deserialize(input: IPayment): this;
}
//# sourceMappingURL=payment.class.d.ts.map