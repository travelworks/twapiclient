import { INotificationBlock } from '../../interfaces/twbooking/notification.block.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CAbstractTimestampClass } from '../abstract.timestamp.class';
export declare class CNotificationBlock extends CAbstractTimestampClass implements INotificationBlock, IDeserializable<CNotificationBlock> {
    notification_rules: string[];
    customer: string;
    block_all: boolean;
    block_companions: boolean;
    block_points: boolean;
    block_surveys: boolean;
    block_payments: boolean;
    block_notifications: boolean;
    deserialize(input: INotificationBlock): CNotificationBlock;
}
//# sourceMappingURL=notification.block.class.d.ts.map