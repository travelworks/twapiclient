"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CPayment = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CPayment = /** @class */ (function (_super) {
    __extends(CPayment, _super);
    function CPayment() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CPayment.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.payment_id = input.payment_id;
        this.open_payment_amount = input.open_payment_amount;
        this.payment_amount = input.payment_amount;
        this.payment_date = input.payment_date;
        this.email_send = input.email_send;
        this.email_id = input.email_id;
        return this;
    };
    return CPayment;
}(abstract_class_1.default));
exports.CPayment = CPayment;
//# sourceMappingURL=payment.class.js.map