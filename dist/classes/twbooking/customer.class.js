"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CCustomer = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var booking_class_1 = require("./booking.class");
var CCustomer = /** @class */ (function (_super) {
    __extends(CCustomer, _super);
    function CCustomer() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.address = new CCustomer.CAddress();
        return _this;
    }
    CCustomer.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.gender = input.gender;
        this.last_name = input.last_name;
        this.first_name = input.first_name;
        this.phone = input.phone;
        this.dob = input.dob;
        this.email = input.email;
        if (input.customer_number) {
            this.customer_number = input.customer_number;
        }
        if (input.address) {
            this.address = new CCustomer.CAddress().deserialize(input.address);
        }
        else {
            this.address = new CCustomer.CAddress();
        }
        this.bookings = [];
        if (input.bookings && input.bookings.length) {
            input.bookings.forEach(function (booking) {
                _this.bookings.push(new booking_class_1.CBooking().deserialize(booking));
            });
        }
        this.point_status = input.point_status;
        this.com_channels = [];
        if (input.com_channels && input.com_channels.length) {
            input.com_channels.forEach(function (entry) {
                _this.com_channels.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.customer_properties = [];
        if (input.customer_properties && input.customer_properties.length) {
            input.customer_properties.forEach(function (entry) {
                _this.customer_properties.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.external_references = [];
        if (input.external_references && input.external_references.length) {
            input.external_references.forEach(function (entry) {
                _this.external_references.push(new CCustomer.CAdditionalAttribute().deserialize(entry));
            });
        }
        this.additional_emails = [];
        if (input.additional_emails && input.additional_emails.length) {
            input.additional_emails.forEach(function (entry) {
                _this.additional_emails.push(new CCustomer.CEmail().deserialize(entry));
            });
        }
        return this;
    };
    CCustomer.prototype.getAge = function () {
        var ageDifMs = Date.now() - new Date(this.dob).getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    };
    return CCustomer;
}(abstract_class_1.default));
exports.CCustomer = CCustomer;
(function (CCustomer) {
    var CAddress = /** @class */ (function () {
        function CAddress() {
        }
        CAddress.prototype.deserialize = function (input) {
            this.additional = input.additional;
            this.city = input.city;
            this.country = input.country;
            this.street = input.street;
            this.zip = input.zip;
            return this;
        };
        return CAddress;
    }());
    CCustomer.CAddress = CAddress;
    var CEmail = /** @class */ (function () {
        function CEmail() {
        }
        CEmail.prototype.deserialize = function (input) {
            this.email = input.email;
            this.description = input.description;
            return this;
        };
        return CEmail;
    }());
    CCustomer.CEmail = CEmail;
    var CAdditionalAttribute = /** @class */ (function (_super) {
        __extends(CAdditionalAttribute, _super);
        function CAdditionalAttribute() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CAdditionalAttribute.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.code = input.code;
            this.title = input.title;
            this.valid_from = input.valid_from;
            this.valid_to = input.valid_to;
            this.value = input.value;
            return this;
        };
        return CAdditionalAttribute;
    }(abstract_class_1.default));
    CCustomer.CAdditionalAttribute = CAdditionalAttribute;
    var CNote = /** @class */ (function (_super) {
        __extends(CNote, _super);
        function CNote() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CNote.prototype.deserialize = function (input) {
            _super.prototype.deserialize.call(this, input);
            this.customer = input.customer;
            this.contact_method = input.contact_method;
            this.contact_person = input.contact_person;
            this.content = input.content;
            this.createdAt = input.createdAt;
            this.updatedAt = input.updatedAt;
            this.type = input.type;
            this.probability = input.probability;
            return this;
        };
        return CNote;
    }(abstract_class_1.default));
    CCustomer.CNote = CNote;
})(CCustomer = exports.CCustomer || (exports.CCustomer = {}));
exports.CCustomer = CCustomer;
//# sourceMappingURL=customer.class.js.map