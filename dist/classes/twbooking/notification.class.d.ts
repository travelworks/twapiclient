import { INotification } from '../../interfaces/twbooking/notification.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { CAbstractTimestampClass } from '../abstract.timestamp.class';
export declare class CNotification extends CAbstractTimestampClass implements INotification, IDeserializable<CNotification> {
    email: string;
    notification_rule: string;
    customer: string;
    booking: string;
    payment: string;
    payment_id: string;
    payment_amount: number;
    payment_date: Date;
    deserialize(input: INotification): CNotification;
}
//# sourceMappingURL=notification.class.d.ts.map