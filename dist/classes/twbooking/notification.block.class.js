"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CNotificationBlock = void 0;
var abstract_timestamp_class_1 = require("../abstract.timestamp.class");
var CNotificationBlock = /** @class */ (function (_super) {
    __extends(CNotificationBlock, _super);
    function CNotificationBlock() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CNotificationBlock.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.notification_rules = input.notification_rules;
        this.customer = input.customer;
        this.block_all = input.block_all;
        this.block_companions = input.block_companions;
        this.block_points = input.block_points;
        this.block_surveys = input.block_surveys;
        this.block_payments = input.block_payments;
        this.block_notifications = input.block_notifications;
        return this;
    };
    return CNotificationBlock;
}(abstract_timestamp_class_1.CAbstractTimestampClass));
exports.CNotificationBlock = CNotificationBlock;
//# sourceMappingURL=notification.block.class.js.map