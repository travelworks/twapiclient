"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CNotificationRule = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CNotificationRule = /** @class */ (function (_super) {
    __extends(CNotificationRule, _super);
    function CNotificationRule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CNotificationRule.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.conditions = input.conditions;
        this.email_template = input.email_template;
        this.name = input.name;
        this.active = input.active;
        return this;
    };
    return CNotificationRule;
}(abstract_class_1.default));
exports.CNotificationRule = CNotificationRule;
//# sourceMappingURL=notification.rule.class.js.map