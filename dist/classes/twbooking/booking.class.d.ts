import CAbstractClass from '../abstract.class';
import { IBooking } from '../../interfaces/twbooking/booking.interface';
import { CPaymentReminder } from './paymentReminder.class';
import { CPayment } from './payment.class';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import IProgramCity = IBooking.IProgramCity;
import { CCustomer } from './customer.class';
export declare class CBooking extends CAbstractClass implements IBooking, IDeserializable<CBooking> {
    booking_number: string;
    booking_status: number;
    last_modified: Date;
    payment_reminders: CPaymentReminder[];
    payment_status: number;
    payments: CPayment[];
    payment: CBooking.CBookingPayment;
    product_code: string;
    program_city: CBooking.CProgramCity;
    program_code: string;
    program_country: string;
    program_end: Date;
    program_duration: number;
    program_name: string;
    program_start: Date;
    web_code: string;
    insurance_type: string;
    insurance_start: Date;
    insurance_end: Date;
    travel_begin: string;
    booking_properties: CBooking.CBookingProperty[];
    last_modified_details: CBooking.CBookingLastModifiedDetails;
    booking_notes: CBooking.CBookingNote[];
    customer?: string | CCustomer;
    deserialize(input: IBooking): CBooking;
}
export declare namespace CBooking {
    class CProgramCity extends CAbstractClass implements IProgramCity, IDeserializable<CProgramCity> {
        lat: number;
        lng: number;
        name: string;
        deserialize(input: any): CProgramCity;
    }
    class CBookingPayment extends CAbstractClass implements IBooking.IBookingPayment, IDeserializable<CBookingPayment> {
        open_payment_amount: number;
        payments: CPayment[];
        deserialize(input: IBooking.IBookingPayment): CBookingPayment;
    }
    class CBookingLastModifiedDetails extends CAbstractClass implements IBooking.IBookingLastModifiedDetails, IDeserializable<CBookingLastModifiedDetails> {
        user_name: string;
        user_email: string;
        last_payment_date: Date;
        last_booking_property_date: Date;
        deserialize(input: IBooking.IBookingLastModifiedDetails): CBookingLastModifiedDetails;
    }
    class CBookingNote extends CAbstractClass implements CBooking.CBookingNote, IDeserializable<CBookingNote> {
        content: string;
        deserialize(input: IBooking.IBookingNote): CBookingNote;
    }
    class CBookingProperty extends CAbstractClass implements IBooking.IBookingProperty, IDeserializable<CBookingProperty> {
        value: any;
        code: string;
        type: number;
        title: string;
        last_modified: Date;
        deserialize(input: IBooking.IBookingProperty): CBookingProperty;
        toString(): any;
    }
}
//# sourceMappingURL=booking.class.d.ts.map