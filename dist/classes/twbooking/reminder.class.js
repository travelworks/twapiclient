"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CReminder = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var customer_class_1 = require("./customer.class");
var CReminder = /** @class */ (function (_super) {
    __extends(CReminder, _super);
    function CReminder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CReminder.prototype.deserialize = function (input) {
        _super.prototype.deserialize.call(this, input);
        this.comment = input.comment;
        this.customer_id = input.customer_id;
        this.date = input.date;
        this.reminder_type = input.reminder_type;
        this.user = input.user;
        this.email_send = input.email_send;
        if (input.customer) {
            this.customer = new customer_class_1.CCustomer().deserialize(input.customer);
        }
        return this;
    };
    return CReminder;
}(abstract_class_1.default));
exports.CReminder = CReminder;
//# sourceMappingURL=reminder.class.js.map