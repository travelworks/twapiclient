"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CTrip = void 0;
var CTrip = /** @class */ (function () {
    function CTrip() {
    }
    CTrip.prototype.deserialize = function (input) {
        this._id = input._id;
        this.product_code = input.product_code;
        this.program_code = input.program_code;
        this.program_country = input.program_country;
        this.program_city = input.program_city;
        this.program_end = input.program_end;
        this.program_start = input.program_start;
        return this;
    };
    return CTrip;
}());
exports.CTrip = CTrip;
//# sourceMappingURL=trip.class.js.map