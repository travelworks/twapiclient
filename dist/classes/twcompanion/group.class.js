"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CGroup = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var CGroup = /** @class */ (function (_super) {
    __extends(CGroup, _super);
    function CGroup() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.countries = [];
        _this.products = [];
        return _this;
    }
    CGroup.prototype.deserialize = function (input) {
        var _this = this;
        this._id = input._id;
        this.name = input.name;
        this.countries = [];
        if (input.countries && input.countries.length) {
            input.countries.forEach(function (country) {
                _this.countries.push(country);
            });
        }
        this.products = [];
        if (input.products && input.products.length) {
            input.products.forEach(function (product) {
                _this.products.push(product);
            });
        }
        this.show_map = input.show_map;
        return this;
    };
    return CGroup;
}(abstract_class_1.default));
exports.CGroup = CGroup;
//# sourceMappingURL=group.class.js.map