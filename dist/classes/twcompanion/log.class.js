"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CLog = void 0;
var CLog = /** @class */ (function () {
    function CLog() {
    }
    CLog.prototype.deserialize = function (input) {
        this._id = input._id;
        this.shared_field_name = input.shared_field_name;
        this.old_value = input.old_value;
        this.new_value = input.new_value;
        this.timestamp = input.timestamp;
        return this;
    };
    return CLog;
}());
exports.CLog = CLog;
//# sourceMappingURL=log.class.js.map