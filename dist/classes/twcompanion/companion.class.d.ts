import { ITrip } from '../../interfaces/twcompanion/trip.interface';
import { IDeserializable } from '../../interfaces/deserializable.interface';
import { ICompanion } from '../../interfaces/twcompanion/companion.interface';
import { ISharedField } from '../../interfaces/twcompanion/sharedfield.interface';
import { ILog } from '../../interfaces/twcompanion/log.interface';
import CAbstractClass from '../abstract.class';
export declare class CCompanion extends CAbstractClass implements ICompanion, IDeserializable<CCompanion> {
    _id: string;
    customer: string;
    trips: ITrip[];
    hash: string;
    shared_fields: ISharedField.IField[];
    log: ILog[];
    mails_send: any[];
    invite_send_date: Date;
    reminder_send_date: Date;
    last_login: Date;
    deserialize(input: ICompanion): CCompanion;
}
//# sourceMappingURL=companion.class.d.ts.map