import { IGroup } from "../../interfaces/twcompanion/group.interface";
import { IDeserializable } from "../../interfaces/deserializable.interface";
import CAbstractClass from "../abstract.class";
export declare class CGroup extends CAbstractClass implements IGroup, IDeserializable<CGroup> {
    _id: string;
    name: string;
    countries: string[];
    products: string[];
    show_map: boolean;
    deserialize(input: any): CGroup;
}
//# sourceMappingURL=group.class.d.ts.map