import { IDeserializable } from '../../interfaces/deserializable.interface';
import { ILog } from '../../interfaces/twcompanion/log.interface';
export declare class CLog implements IDeserializable<CLog>, ILog {
    _id: string;
    shared_field_name: string;
    old_value: string;
    new_value: string;
    timestamp: Date;
    deserialize(input: any): CLog;
}
//# sourceMappingURL=log.class.d.ts.map