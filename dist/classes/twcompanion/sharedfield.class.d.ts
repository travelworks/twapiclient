import { IDeserializable } from "../../interfaces/deserializable.interface";
import { ISharedField } from "../../interfaces/twcompanion/sharedfield.interface";
export declare class CSharedFieldConfig implements ISharedField.IConfig, IDeserializable<CSharedFieldConfig> {
    name: string;
    shared: boolean;
    import_function: boolean;
    title: any;
    deserialize(input: any): CSharedFieldConfig;
}
export declare class CSharedField implements ISharedField.IField, IDeserializable<CSharedField> {
    _id: string;
    name: string;
    shared: boolean;
    value: any;
    config: CSharedFieldConfig;
    timestamp: Date;
    _previousShared: boolean;
    deserialize(input: any): CSharedField;
}
//# sourceMappingURL=sharedfield.class.d.ts.map