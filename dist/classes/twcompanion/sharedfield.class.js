"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CSharedField = exports.CSharedFieldConfig = void 0;
var CSharedFieldConfig = /** @class */ (function () {
    function CSharedFieldConfig() {
    }
    CSharedFieldConfig.prototype.deserialize = function (input) {
        this.name = input.name;
        this.shared = input.shared;
        this.import_function = input.import_function;
        this.title = input.title;
        return this;
    };
    return CSharedFieldConfig;
}());
exports.CSharedFieldConfig = CSharedFieldConfig;
var CSharedField = /** @class */ (function () {
    function CSharedField() {
    }
    CSharedField.prototype.deserialize = function (input) {
        this._id = input._id;
        this.name = input.name;
        this.shared = input.shared;
        this.value = (input.value) ? input.value : null;
        return this;
    };
    return CSharedField;
}());
exports.CSharedField = CSharedField;
//# sourceMappingURL=sharedfield.class.js.map