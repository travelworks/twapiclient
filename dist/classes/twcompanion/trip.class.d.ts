import { IDeserializable } from "../../interfaces/deserializable.interface";
import { ITrip } from "../../interfaces/twcompanion/trip.interface";
import { IBooking } from "../../interfaces/twbooking/booking.interface";
export declare class CTrip implements ITrip, IDeserializable<CTrip> {
    _id: string;
    booking: string;
    product_code: string;
    program_code: string;
    program_country: string;
    program_city: IBooking.IProgramCity;
    program_end: Date;
    program_start: Date;
    deserialize(input: any): CTrip;
}
//# sourceMappingURL=trip.class.d.ts.map