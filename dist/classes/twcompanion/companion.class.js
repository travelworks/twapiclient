"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CCompanion = void 0;
var abstract_class_1 = __importDefault(require("../abstract.class"));
var trip_class_1 = require("./trip.class");
var sharedfield_class_1 = require("./sharedfield.class");
var log_class_1 = require("./log.class");
var CCompanion = /** @class */ (function (_super) {
    __extends(CCompanion, _super);
    function CCompanion() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CCompanion.prototype.deserialize = function (input) {
        var _this = this;
        _super.prototype.deserialize.call(this, input);
        this.customer = input.customer;
        this.trips = [];
        if (input.trips && input.trips.length) {
            input.trips.forEach(function (itrip) {
                _this.trips.push(new trip_class_1.CTrip().deserialize(itrip));
            });
        }
        this.hash = input.hash;
        this.shared_fields = [];
        if (input.shared_fields && input.shared_fields.length) {
            input.shared_fields.forEach(function (isf) {
                _this.shared_fields.push(new sharedfield_class_1.CSharedField().deserialize(isf));
            });
        }
        this.log = [];
        if (input.log && input.log.length) {
            input.log.forEach(function (log) {
                _this.log.push(new log_class_1.CLog().deserialize(log));
            });
        }
        this.mails_send = input.mails_send;
        this.invite_send_date = input.invite_send_date;
        this.reminder_send_date = input.reminder_send_date;
        this.last_login = input.last_login;
        return this;
    };
    return CCompanion;
}(abstract_class_1.default));
exports.CCompanion = CCompanion;
//# sourceMappingURL=companion.class.js.map