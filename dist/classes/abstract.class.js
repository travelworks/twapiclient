"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CAbstractClass = /** @class */ (function () {
    function CAbstractClass() {
    }
    CAbstractClass.deserialize = function (inputs) {
        var result = [];
        for (var _i = 0, inputs_1 = inputs; _i < inputs_1.length; _i++) {
            var input = inputs_1[_i];
            var object = new this;
            result.push(object.deserialize(input));
        }
        return result;
    };
    CAbstractClass.prototype.deserialize = function (input) {
        this._id = input._id;
        return this;
    };
    return CAbstractClass;
}());
exports.default = CAbstractClass;
//# sourceMappingURL=abstract.class.js.map