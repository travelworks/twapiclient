export interface IDeserializable<T> {
    deserialize(input: any): T;
}
//# sourceMappingURL=deserializable.interface.d.ts.map