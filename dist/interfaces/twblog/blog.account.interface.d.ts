import IAbstractInterface from '../abstract.interface';
import IAbstractTimeStampInterface from '../abstract.timestamp.interface';
export interface IBlogAccount extends IAbstractInterface, IAbstractTimeStampInterface {
    customer: IBlogAccount.IBlogCustomer;
    hash?: string;
    notifications?: IBlogAccount.IBlogNotification[];
    user_name?: string;
}
export declare namespace IBlogAccount {
    interface IBlogCustomer extends IAbstractInterface {
        customer_number: string;
        first_name: string;
        last_name: string;
    }
    interface IBlogNotification extends IAbstractInterface {
        type: IBlogNotification.Type;
        email: string;
        createdAt: Date;
        updatedAt: Date;
    }
    namespace IBlogNotification {
        enum Type {
            invite = "invite"
        }
    }
}
//# sourceMappingURL=blog.account.interface.d.ts.map