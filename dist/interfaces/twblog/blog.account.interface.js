"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IBlogAccount = void 0;
var IBlogAccount;
(function (IBlogAccount) {
    var IBlogNotification;
    (function (IBlogNotification) {
        var Type;
        (function (Type) {
            Type["invite"] = "invite";
        })(Type = IBlogNotification.Type || (IBlogNotification.Type = {}));
    })(IBlogNotification = IBlogAccount.IBlogNotification || (IBlogAccount.IBlogNotification = {}));
})(IBlogAccount = exports.IBlogAccount || (exports.IBlogAccount = {}));
//# sourceMappingURL=blog.account.interface.js.map