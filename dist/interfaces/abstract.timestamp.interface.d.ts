import IAbstractInterface from './abstract.interface';
export default interface IAbstractTimeStampInterface extends IAbstractInterface {
    createdAt: Date;
    updatedAt: Date;
}
//# sourceMappingURL=abstract.timestamp.interface.d.ts.map