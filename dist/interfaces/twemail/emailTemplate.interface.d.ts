import IAbstractInterface from '../abstract.interface';
export interface IEmailTemplate extends IAbstractInterface {
    name: string;
    from: string;
    subject: string;
    text: string;
    html: string;
    placeholders: IEmailTemplate.IPlaceholder[];
    type: string;
    attachments: IAttachment[];
}
export interface IAttachment extends IAbstractInterface {
    meta: any;
    data: any;
}
export declare namespace IEmailTemplate {
    enum IPlaceholderType {
        salutation = "salutation",
        lovelySalutation = "lovelySalutation",
        currency = "currency",
        date = "date"
    }
    interface IPlaceholder {
        path: string;
        type: IPlaceholderType;
    }
}
//# sourceMappingURL=emailTemplate.interface.d.ts.map