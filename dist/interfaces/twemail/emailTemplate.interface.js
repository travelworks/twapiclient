"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IEmailTemplate = void 0;
var IEmailTemplate;
(function (IEmailTemplate) {
    var IPlaceholderType;
    (function (IPlaceholderType) {
        IPlaceholderType["salutation"] = "salutation";
        IPlaceholderType["lovelySalutation"] = "lovelySalutation";
        IPlaceholderType["currency"] = "currency";
        IPlaceholderType["date"] = "date";
    })(IPlaceholderType = IEmailTemplate.IPlaceholderType || (IEmailTemplate.IPlaceholderType = {}));
})(IEmailTemplate = exports.IEmailTemplate || (exports.IEmailTemplate = {}));
//# sourceMappingURL=emailTemplate.interface.js.map