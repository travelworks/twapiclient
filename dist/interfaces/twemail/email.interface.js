"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IEmail = void 0;
var IEmail;
(function (IEmail) {
    var SendStatus;
    (function (SendStatus) {
        SendStatus["new"] = "new";
        SendStatus["send"] = "send";
        SendStatus["error"] = "error";
    })(SendStatus = IEmail.SendStatus || (IEmail.SendStatus = {}));
})(IEmail = exports.IEmail || (exports.IEmail = {}));
//# sourceMappingURL=email.interface.js.map