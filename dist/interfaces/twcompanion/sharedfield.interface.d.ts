import IAbstractInterface from "../abstract.interface";
import { IDeserializable } from "../deserializable.interface";
export declare namespace ISharedField {
    interface IField extends IAbstractInterface {
        _id: string;
        name: string;
        shared: boolean;
        value: any;
        config: IConfig;
        timestamp: Date;
        _previousShared: boolean;
    }
    interface IConfig extends IDeserializable<IConfig> {
        name: string;
        shared: boolean;
        import_function: boolean;
        title: any;
    }
}
//# sourceMappingURL=sharedfield.interface.d.ts.map