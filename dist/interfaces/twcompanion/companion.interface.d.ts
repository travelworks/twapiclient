import IAbstractInterface from '../abstract.interface';
import { ITrip } from './trip.interface';
import { ISharedField } from './sharedfield.interface';
import { ILog } from './log.interface';
export interface ICompanion extends IAbstractInterface {
    customer: string;
    trips: ITrip[];
    hash: string;
    shared_fields: ISharedField.IField[];
    log: ILog[];
    mails_send: any[];
    invite_send_date: Date;
    reminder_send_date: Date;
    last_login: Date;
}
//# sourceMappingURL=companion.interface.d.ts.map