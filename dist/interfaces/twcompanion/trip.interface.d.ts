import IAbstractInterface from "../abstract.interface";
import { IBooking } from "../twbooking/booking.interface";
export interface ITrip extends IAbstractInterface {
    booking: string;
    program_code: string;
    product_code: string;
    program_country: string;
    program_city: IBooking.IProgramCity;
    program_start: Date;
    program_end: Date;
}
//# sourceMappingURL=trip.interface.d.ts.map