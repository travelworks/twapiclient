"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IPointAccount = void 0;
var IPointAccount;
(function (IPointAccount) {
    var IPointNotification;
    (function (IPointNotification) {
        var Type;
        (function (Type) {
            Type["invite"] = "invite";
            Type["approval"] = "approval";
            Type["rejection"] = "rejection";
            Type["deletion"] = "deletion";
        })(Type = IPointNotification.Type || (IPointNotification.Type = {}));
    })(IPointNotification = IPointAccount.IPointNotification || (IPointAccount.IPointNotification = {}));
    var Status;
    (function (Status) {
        Status["open"] = "open";
        Status["pending"] = "pending";
        Status["approved"] = "approved";
        Status["rejected"] = "rejected";
    })(Status = IPointAccount.Status || (IPointAccount.Status = {}));
})(IPointAccount = exports.IPointAccount || (exports.IPointAccount = {}));
//# sourceMappingURL=point.account.interface.js.map