import IAbstractInterface from '../abstract.interface';
export interface IPointAccount extends IAbstractInterface {
    customer: string;
    hash: string;
    entries: IPointAccount.IPointEntry[];
    createdAt: Date;
    updatedAt: Date;
    status: IPointAccount.Status;
    status_comment: string;
    current_balance: number;
    notifications: IPointAccount.IPointNotification[];
}
export declare namespace IPointAccount {
    interface IPointNotification extends IAbstractInterface {
        type: IPointNotification.Type;
        email: string;
        createdAt: Date;
        updatedAt: Date;
    }
    namespace IPointNotification {
        enum Type {
            invite = "invite",
            approval = "approval",
            rejection = "rejection",
            deletion = "deletion"
        }
    }
    interface IPointEntry extends IAbstractInterface {
        amount: number;
        createdAt: Date;
        updatedAt: Date;
        booking: string;
        point_activity: string;
        issued_by: string;
    }
    enum Status {
        'open' = "open",
        'pending' = "pending",
        'approved' = "approved",
        'rejected' = "rejected"
    }
}
//# sourceMappingURL=point.account.interface.d.ts.map