import IAbstractInterface from '../abstract.interface';
export interface IPointActivity extends IAbstractInterface {
    name: string;
    amount: number;
}
//# sourceMappingURL=point.activity.interface.d.ts.map