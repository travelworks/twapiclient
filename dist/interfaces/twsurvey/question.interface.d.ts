import IAbstractInterface from '../abstract.interface';
import { IOption } from './option.interface';
export interface IQuestion extends IAbstractInterface {
    keyword?: string;
    name?: string;
    text: string;
    type: IQuestion.Types;
    input_type: IQuestion.InputTypes;
    options?: IOption[];
    answer?: any;
    conditions?: IOption[];
    limit?: number;
    required: boolean;
    questions: IQuestion[];
}
export declare namespace IQuestion {
    enum Types {
        open = "open",
        openblock = "openblock",
        headline = "headline",
        choice = "choice",
        multichoice = "multichoice",
        rating = "rating",
        starrating = "starrating"
    }
    enum InputTypes {
        text = "text",
        date = "date",
        email = "email",
        color = "color",
        range = "range"
    }
}
//# sourceMappingURL=question.interface.d.ts.map