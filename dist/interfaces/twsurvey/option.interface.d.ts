import IAbstractInterface from '../abstract.interface';
export interface IOption extends IAbstractInterface {
    label: string;
    value: string;
}
//# sourceMappingURL=option.interface.d.ts.map