"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IQuestion = void 0;
var IQuestion;
(function (IQuestion) {
    var Types;
    (function (Types) {
        Types["open"] = "open";
        Types["openblock"] = "openblock";
        Types["headline"] = "headline";
        Types["choice"] = "choice";
        Types["multichoice"] = "multichoice";
        Types["rating"] = "rating";
        Types["starrating"] = "starrating";
    })(Types = IQuestion.Types || (IQuestion.Types = {}));
    var InputTypes;
    (function (InputTypes) {
        InputTypes["text"] = "text";
        InputTypes["date"] = "date";
        InputTypes["email"] = "email";
        InputTypes["color"] = "color";
        InputTypes["range"] = "range";
    })(InputTypes = IQuestion.InputTypes || (IQuestion.InputTypes = {}));
})(IQuestion = exports.IQuestion || (exports.IQuestion = {}));
//# sourceMappingURL=question.interface.js.map