import IAbstractInterface from '../abstract.interface';
import { ISurvey } from './survey.interface';
import { ICustomer } from '../../interfaces/twbooking/customer.interface';
import { IBooking } from '../../interfaces/twbooking/booking.interface';
export interface IRequest extends IAbstractInterface {
    survey?: any;
    hash?: string;
    response: ISurvey;
    status: string;
    booking: any;
    customer: any;
    email_status?: IRequest.IEMailStatus;
    internal: IRequest.IInternal;
    is_new: boolean;
    createdAt: Date;
    updatedAt: Date;
}
export declare namespace IRequest {
    interface IEMailStatus {
        firstEmail?: any;
        secondEmail?: any;
    }
    interface IInternal {
        customer: ICustomer;
        booking: IBooking;
    }
}
//# sourceMappingURL=request.interface.d.ts.map