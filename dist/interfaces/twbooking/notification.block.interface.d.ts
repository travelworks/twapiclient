import IAbstractTimeStampInterface from '../abstract.timestamp.interface';
export interface INotificationBlock extends IAbstractTimeStampInterface {
    customer: string;
    notification_rules?: string[];
    block_all: boolean;
    block_surveys: boolean;
    block_companions: boolean;
    block_points: boolean;
    block_payments: boolean;
    block_notifications: boolean;
}
//# sourceMappingURL=notification.block.interface.d.ts.map