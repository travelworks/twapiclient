import IAbstractInterface from '../abstract.interface';
export interface INotificationRule extends IAbstractInterface {
    name: string;
    email_template: string;
    conditions: INotificationRule.ICondition[];
    active: boolean;
}
export declare namespace INotificationRule {
    interface ICondition {
        target: string;
        type: ICondition.Type;
        operator: ICondition.Operator;
        value: any;
    }
    namespace ICondition {
        enum Type {
            string = "string",
            date = "date"
        }
        enum Operator {
            eq = "eq",
            gte = "gte",
            lte = "lte",
            in = "in",
            nin = "nin",
            ne = "ne"
        }
    }
}
//# sourceMappingURL=notification.rule.interface.d.ts.map