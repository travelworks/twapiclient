import IAbstractTimeStampInterface from '../abstract.timestamp.interface';
export interface INotification extends IAbstractTimeStampInterface {
    notification_rule: string;
    email: string;
    customer: string;
    booking: string;
    payment: string;
    payment_id: string;
    payment_amount: number;
    payment_date: Date;
}
//# sourceMappingURL=notification.interface.d.ts.map