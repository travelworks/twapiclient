import IAbstractInterface from '../abstract.interface';
export interface IPaymentReminder extends IAbstractInterface {
    payment_id: string;
    reminder_type: string;
    reminder_value: string;
    email_send: boolean;
    email_id: string;
    payment_amount: number;
}
//# sourceMappingURL=paymentReminder.interface.d.ts.map