"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IReminder = void 0;
var IReminder;
(function (IReminder) {
    var Type;
    (function (Type) {
        Type["followup"] = "followup";
    })(Type = IReminder.Type || (IReminder.Type = {}));
})(IReminder = exports.IReminder || (exports.IReminder = {}));
//# sourceMappingURL=reminder.interface.js.map