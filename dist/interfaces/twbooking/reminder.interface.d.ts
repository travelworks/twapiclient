import IAbstractInterface from '../abstract.interface';
import { ICustomer } from './customer.interface';
export interface IReminder extends IAbstractInterface {
    date: Date;
    customer_id: string;
    user: {
        _id: string;
        name: string;
        email: string;
    };
    reminder_type: IReminder.Type;
    comment: string;
    email_send?: string;
    customer?: ICustomer;
}
export declare namespace IReminder {
    enum Type {
        followup = "followup"
    }
}
//# sourceMappingURL=reminder.interface.d.ts.map