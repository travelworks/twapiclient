import IAbstractInterface from '../abstract.interface';
import { IBooking } from './booking.interface';
import { IPointAccount } from '../..';
export declare namespace ICustomer {
    enum Gender {
        male = "male",
        female = "female",
        diverse = "diverse"
    }
    enum SubType {
        customer = "customer",
        applicant = "applicant",
        other = "other"
    }
    interface IAddress {
        street: string;
        zip: string;
        city: string;
        country: string;
        additional: string;
    }
    interface IEmail {
        email: string;
        description: string;
    }
    interface INote extends IAbstractInterface {
        customer: string;
        createdAt: Date;
        updatedAt: Date;
        type: INote.Type;
        contact_method: INote.ContactType;
        contact_person: string;
        content: string;
        probability?: INote.Probability;
    }
    namespace INote {
        enum Probability {
            likely = "likely",
            maybe = "maybe",
            unlikely = "unlikely"
        }
        enum Type {
            initial_inquiry = "initial_inquiry",
            consultation = "consultation",
            offer = "offer",
            complaint = "complaint"
        }
        enum ContactType {
            email = "email",
            phone = "phone",
            web = "web",
            letter = "letter"
        }
    }
    interface IAdditionalAttribute extends IAbstractInterface {
        value: string;
        code: string;
        title: string;
        valid_from: Date;
        valid_to: Date;
    }
}
export interface ICustomer extends IAbstractInterface {
    gender: ICustomer.Gender;
    last_name: string;
    first_name: string;
    email: string;
    additional_emails: ICustomer.IEmail[];
    phone: string;
    mobile: string;
    dob: Date;
    customer_number?: string;
    address: ICustomer.IAddress;
    bookings?: IBooking[];
    invoice_email?: string;
    customer_last_modified: Date;
    sub_type: ICustomer.SubType;
    point_status: IPointAccount.Status;
    com_channels: ICustomer.IAdditionalAttribute[];
    customer_properties: ICustomer.IAdditionalAttribute[];
    external_references: ICustomer.IAdditionalAttribute[];
}
//# sourceMappingURL=customer.interface.d.ts.map