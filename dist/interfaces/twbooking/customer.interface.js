"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ICustomer = void 0;
var ICustomer;
(function (ICustomer) {
    var Gender;
    (function (Gender) {
        Gender["male"] = "male";
        Gender["female"] = "female";
        Gender["diverse"] = "diverse";
    })(Gender = ICustomer.Gender || (ICustomer.Gender = {}));
    var SubType;
    (function (SubType) {
        SubType["customer"] = "customer";
        SubType["applicant"] = "applicant";
        SubType["other"] = "other";
    })(SubType = ICustomer.SubType || (ICustomer.SubType = {}));
    var INote;
    (function (INote) {
        var Probability;
        (function (Probability) {
            Probability["likely"] = "likely";
            Probability["maybe"] = "maybe";
            Probability["unlikely"] = "unlikely";
        })(Probability = INote.Probability || (INote.Probability = {}));
        var Type;
        (function (Type) {
            Type["initial_inquiry"] = "initial_inquiry";
            Type["consultation"] = "consultation";
            Type["offer"] = "offer";
            Type["complaint"] = "complaint";
        })(Type = INote.Type || (INote.Type = {}));
        var ContactType;
        (function (ContactType) {
            ContactType["email"] = "email";
            ContactType["phone"] = "phone";
            ContactType["web"] = "web";
            ContactType["letter"] = "letter";
        })(ContactType = INote.ContactType || (INote.ContactType = {}));
    })(INote = ICustomer.INote || (ICustomer.INote = {}));
})(ICustomer = exports.ICustomer || (exports.ICustomer = {}));
//# sourceMappingURL=customer.interface.js.map