"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.INotificationRule = void 0;
var INotificationRule;
(function (INotificationRule) {
    var ICondition;
    (function (ICondition) {
        var Type;
        (function (Type) {
            Type["string"] = "string";
            Type["date"] = "date";
        })(Type = ICondition.Type || (ICondition.Type = {}));
        var Operator;
        (function (Operator) {
            Operator["eq"] = "eq";
            Operator["gte"] = "gte";
            Operator["lte"] = "lte";
            Operator["in"] = "in";
            Operator["nin"] = "nin";
            Operator["ne"] = "ne";
        })(Operator = ICondition.Operator || (ICondition.Operator = {}));
    })(ICondition = INotificationRule.ICondition || (INotificationRule.ICondition = {}));
})(INotificationRule = exports.INotificationRule || (exports.INotificationRule = {}));
//# sourceMappingURL=notification.rule.interface.js.map