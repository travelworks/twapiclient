import TwAbstractApiClient from './twAbstractApiClient';
import { CPointAccount } from '../classes/twpoint/point.account.class';
import { CPointActivity } from '../classes/twpoint/point.activity.class';
import TwMinApiClient from './twMinApiClient';
export declare namespace TwPointClient {
    namespace Private {
        import CPointEntry = CPointAccount.CPointEntry;
        class PointAccountClient<T = CPointAccount> extends TwAbstractApiClient<T> {
            protected readonly path = "accounts";
            Class: typeof CPointAccount;
            postEntry(entry: CPointAccount.CPointEntry, customerID: string): Promise<CPointEntry>;
            deleteEntry(entryID: string, customerID: string): Promise<CPointEntry>;
        }
        class PointActivityClient<T = CPointActivity> extends TwAbstractApiClient<T> {
            protected readonly path = "activities";
            Class: typeof CPointActivity;
        }
    }
    namespace Public {
        class PointAccountClient<T = CPointAccount> extends TwMinApiClient<T> {
            protected readonly path = "public/accounts";
            Class: typeof CPointAccount;
            get(hash: string): Promise<T>;
            postStatus(status: any, hash: string): Promise<T>;
        }
    }
}
//# sourceMappingURL=twPoint.client.d.ts.map