"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var twMinApiClient_1 = __importDefault(require("./twMinApiClient"));
var TwAbstractApiClient = /** @class */ (function (_super) {
    __extends(TwAbstractApiClient, _super);
    function TwAbstractApiClient() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TwAbstractApiClient.prototype.list = function (query, sorting, paging, projection, path) {
        var _this = this;
        if (query === void 0) { query = null; }
        if (sorting === void 0) { sorting = null; }
        if (paging === void 0) { paging = null; }
        if (projection === void 0) { projection = null; }
        if (path === void 0) { path = null; }
        var queryPath = '';
        if (query || sorting || paging) {
            queryPath += '?';
            var params = [];
            if (Object.keys(query).length) {
                params.push('q=' + encodeURIComponent(JSON.stringify(query)));
            }
            if (sorting) {
                params.push('s=' + encodeURIComponent(JSON.stringify(sorting)));
            }
            if (paging) {
                params.push('p=' + encodeURIComponent(JSON.stringify(paging)));
            }
            if (projection) {
                params.push('o=' + encodeURIComponent(JSON.stringify(projection)));
            }
            queryPath += params.join('&');
        }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            options.method = 'get';
            options.url += queryPath;
            (0, axios_1.default)(options).then(function (res) {
                var twListResult = {
                    items: res.data,
                    totalCount: parseInt(res.headers['x-total-count'], 10) || 0
                };
                if (_this.Class) {
                    twListResult.items = _this.Class.deserialize(res.data);
                }
                resolve(twListResult);
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    TwAbstractApiClient.prototype.get = function (id, path) {
        var _this = this;
        if (path === void 0) { path = null; }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            options.url += id;
            options.method = 'get';
            (0, axios_1.default)(options).then(function (res) {
                resolve(_this.prepareResult(res));
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    TwAbstractApiClient.prototype.post = function (data, path) {
        var _this = this;
        if (path === void 0) { path = null; }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            options.method = 'POST';
            options.data = data;
            (0, axios_1.default)(options).then(function (res) {
                resolve(_this.prepareResult(res));
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    TwAbstractApiClient.prototype.patch = function (data, id, path) {
        var _this = this;
        if (path === void 0) { path = null; }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            id = (id) ? id : data._id;
            options.url += id;
            options.method = 'PATCH';
            options.data = data;
            (0, axios_1.default)(options).then(function (res) {
                resolve(_this.prepareResult(res));
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    TwAbstractApiClient.prototype.put = function (data, id, path) {
        var _this = this;
        if (path === void 0) { path = null; }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            id = (id) ? id : data._id;
            options.url += id;
            options.method = 'PUT';
            options.data = data;
            (0, axios_1.default)(options).then(function (res) {
                resolve(_this.prepareResult(res));
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    TwAbstractApiClient.prototype.delete = function (id, path) {
        var _this = this;
        if (path === void 0) { path = null; }
        return new Promise(function (resolve, reject) {
            var options = _this.getOptions(path);
            options.url += id;
            options.method = 'DELETE';
            (0, axios_1.default)(options).then(function (res) {
                resolve(_this.prepareResult(res));
            }).catch(function (err) {
                reject(err);
            });
        });
    };
    return TwAbstractApiClient;
}(twMinApiClient_1.default));
exports.default = TwAbstractApiClient;
//# sourceMappingURL=twAbstractApiClient.js.map