import TwAbstractApiClient from './twAbstractApiClient';
import { CEmailTemplate } from '../classes/twemail/emailTemplate.class';
import { CEmail } from '../classes/twemail/email.class';
export declare namespace TwEmailClient {
    class EmailClient<T = CEmail> extends TwAbstractApiClient<T> {
        protected readonly path = "emails";
        Class: typeof CEmail;
    }
    class EmailTemplateClient<T = CEmailTemplate> extends TwAbstractApiClient<T> {
        protected readonly path = "emailtemplates";
        Class: typeof CEmailTemplate;
    }
}
//# sourceMappingURL=twEmail.client.d.ts.map