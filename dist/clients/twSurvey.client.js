"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwSurveyClient = void 0;
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var survey_class_1 = require("../classes/twsurvey/survey.class");
var request_class_1 = require("../classes/twsurvey/request.class");
var question_class_1 = require("../classes/twsurvey/question.class");
var twMinApiClient_1 = __importDefault(require("./twMinApiClient"));
var axios_1 = __importDefault(require("axios"));
var TwSurveyClient;
(function (TwSurveyClient) {
    var Private;
    (function (Private) {
        var SurveyClient = /** @class */ (function (_super) {
            __extends(SurveyClient, _super);
            function SurveyClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'surveys';
                _this.Class = survey_class_1.CSurvey;
                return _this;
            }
            return SurveyClient;
        }(twAbstractApiClient_1.default));
        Private.SurveyClient = SurveyClient;
        var RequestClient = /** @class */ (function (_super) {
            __extends(RequestClient, _super);
            function RequestClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'requests';
                _this.Class = request_class_1.CRequest;
                return _this;
            }
            return RequestClient;
        }(twAbstractApiClient_1.default));
        Private.RequestClient = RequestClient;
        var QuestionClient = /** @class */ (function (_super) {
            __extends(QuestionClient, _super);
            function QuestionClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'questions';
                _this.Class = question_class_1.CQuestion;
                return _this;
            }
            return QuestionClient;
        }(twAbstractApiClient_1.default));
        Private.QuestionClient = QuestionClient;
    })(Private = TwSurveyClient.Private || (TwSurveyClient.Private = {}));
    var Public;
    (function (Public) {
        var SurveyClient = /** @class */ (function (_super) {
            __extends(SurveyClient, _super);
            function SurveyClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'public/surveys';
                _this.Class = survey_class_1.CSurvey;
                return _this;
            }
            SurveyClient.prototype.get = function (id) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += id;
                    options.method = 'get';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            return SurveyClient;
        }(twMinApiClient_1.default));
        Public.SurveyClient = SurveyClient;
        var RequestClient = /** @class */ (function (_super) {
            __extends(RequestClient, _super);
            function RequestClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'public/requests';
                _this.Class = request_class_1.CRequest;
                return _this;
            }
            RequestClient.prototype.get = function (hash) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            RequestClient.prototype.put = function (data, id) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    id = (id) ? id : data._id;
                    options.url += id;
                    options.method = 'PUT';
                    options.data = data;
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            return RequestClient;
        }(twMinApiClient_1.default));
        Public.RequestClient = RequestClient;
    })(Public = TwSurveyClient.Public || (TwSurveyClient.Public = {}));
})(TwSurveyClient = exports.TwSurveyClient || (exports.TwSurveyClient = {}));
//# sourceMappingURL=twSurvey.client.js.map