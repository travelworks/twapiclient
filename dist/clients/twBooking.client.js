"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwBookingClient = void 0;
var customer_class_1 = require("../classes/twbooking/customer.class");
var booking_class_1 = require("../classes/twbooking/booking.class");
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var notification_rule_class_1 = require("../classes/twbooking/notification.rule.class");
var axios_1 = __importDefault(require("axios"));
var reminder_class_1 = require("../classes/twbooking/reminder.class");
var notification_block_class_1 = require("../classes/twbooking/notification.block.class");
var notification_class_1 = require("../classes/twbooking/notification.class");
var twMinApiClient_1 = __importDefault(require("./twMinApiClient"));
var TwBookingClient;
(function (TwBookingClient) {
    var CustomerClient = /** @class */ (function (_super) {
        __extends(CustomerClient, _super);
        function CustomerClient() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.path = 'customers';
            _this.Class = customer_class_1.CCustomer;
            return _this;
        }
        CustomerClient.prototype.getBookings = function (query, sorting, paging) {
            var _this = this;
            if (query === void 0) { query = null; }
            if (sorting === void 0) { sorting = null; }
            if (paging === void 0) { paging = null; }
            return new Promise(function (resolve, reject) {
                query['bookings.0'] = { $exists: true };
                _super.prototype.list.call(_this, query, sorting, paging).then(function (customerResponse) {
                    var customerBookings = [];
                    customerResponse.items.forEach(function (customer) {
                        customer.bookings.forEach(function (booking) {
                            customerBookings.push({ customer: customer, booking: booking });
                        });
                    });
                    resolve({ items: customerBookings, totalCount: customerResponse.totalCount });
                });
            });
        };
        return CustomerClient;
    }(twAbstractApiClient_1.default));
    TwBookingClient.CustomerClient = CustomerClient;
    (function (CustomerClient) {
        var AbstractCustomerClient = /** @class */ (function (_super) {
            __extends(AbstractCustomerClient, _super);
            function AbstractCustomerClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'customers';
                return _this;
            }
            AbstractCustomerClient.prototype.list = function (customer_id, query, sorting, paging, projection) {
                if (query === void 0) { query = null; }
                if (sorting === void 0) { sorting = null; }
                if (paging === void 0) { paging = null; }
                if (projection === void 0) { projection = null; }
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.list.call(this, query, sorting, paging, projection, path);
            };
            AbstractCustomerClient.prototype.get = function (customer_id, id) {
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.get.call(this, id, path);
            };
            AbstractCustomerClient.prototype.post = function (customer_id, data) {
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.post.call(this, data, path);
            };
            AbstractCustomerClient.prototype.patch = function (customer_id, data, id) {
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.patch.call(this, data, id, path);
            };
            AbstractCustomerClient.prototype.put = function (customer_id, data, id) {
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.put.call(this, data, id, path);
            };
            AbstractCustomerClient.prototype.delete = function (customer_id, id) {
                var path = this.path + '/' + customer_id.toString() + '/' + this.subPath;
                return _super.prototype.delete.call(this, id, path);
            };
            return AbstractCustomerClient;
        }(twAbstractApiClient_1.default));
        var NotificationClient = /** @class */ (function (_super) {
            __extends(NotificationClient, _super);
            function NotificationClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.subPath = 'notifications';
                _this.Class = notification_class_1.CNotification;
                return _this;
            }
            return NotificationClient;
        }(AbstractCustomerClient));
        CustomerClient.NotificationClient = NotificationClient;
        var NotificationBlockClient = /** @class */ (function (_super) {
            __extends(NotificationBlockClient, _super);
            function NotificationBlockClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.Class = notification_block_class_1.CNotificationBlock;
                return _this;
            }
            NotificationBlockClient.getBlockedStatus = function (block, scope, notification) {
                if (!block) {
                    return false;
                }
                if (block.block_all) {
                    return true;
                }
                switch (scope) {
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.survey:
                        return block.block_surveys;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.companion:
                        return block.block_companions;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.point:
                        return block.block_points;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.payment:
                        return block.block_payments;
                    case TwBookingClient.CustomerClient.NotificationBlockClient.Scopes.notification:
                        if (block.notification_rules
                            && block.notification_rules.length
                            && block.notification_rules.includes(notification)) {
                            return true;
                        }
                        return false;
                        break;
                    default:
                        return false;
                }
            };
            NotificationBlockClient.prototype.get = function (cid) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'GET';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            NotificationBlockClient.prototype.patch = function (cid, data) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'PATCH';
                    options.data = data;
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            NotificationBlockClient.prototype.delete = function (cid) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions('customers/' + cid + '/notifications/block');
                    options.method = 'DELETE';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            NotificationBlockClient.prototype.isBlocked = function (cid, scope, notification) {
                return __awaiter(this, void 0, void 0, function () {
                    var block, e_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                return [4 /*yield*/, this.get(cid)];
                            case 1:
                                block = _a.sent();
                                return [2 /*return*/, NotificationBlockClient.getBlockedStatus(block, scope, notification)];
                            case 2:
                                e_1 = _a.sent();
                                return [2 /*return*/, false];
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            return NotificationBlockClient;
        }(twMinApiClient_1.default));
        CustomerClient.NotificationBlockClient = NotificationBlockClient;
        (function (NotificationBlockClient) {
            var Scopes;
            (function (Scopes) {
                Scopes["survey"] = "survey";
                Scopes["companion"] = "companion";
                Scopes["point"] = "point";
                Scopes["payment"] = "payment";
                Scopes["notification"] = "notification";
            })(Scopes = NotificationBlockClient.Scopes || (NotificationBlockClient.Scopes = {}));
        })(NotificationBlockClient = CustomerClient.NotificationBlockClient || (CustomerClient.NotificationBlockClient = {}));
        var NoteClient = /** @class */ (function (_super) {
            __extends(NoteClient, _super);
            function NoteClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.subPath = 'notes';
                _this.Class = customer_class_1.CCustomer.CNote;
                return _this;
            }
            return NoteClient;
        }(AbstractCustomerClient));
        CustomerClient.NoteClient = NoteClient;
        // tslint:disable-next-line:no-shadowed-variable
        var ReminderClient = /** @class */ (function (_super) {
            __extends(ReminderClient, _super);
            function ReminderClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.subPath = 'reminders';
                _this.Class = reminder_class_1.CReminder;
                return _this;
            }
            return ReminderClient;
        }(AbstractCustomerClient));
        CustomerClient.ReminderClient = ReminderClient;
    })(CustomerClient = TwBookingClient.CustomerClient || (TwBookingClient.CustomerClient = {}));
    var ReminderClient = /** @class */ (function (_super) {
        __extends(ReminderClient, _super);
        function ReminderClient() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.path = 'reminders';
            _this.Class = reminder_class_1.CReminder;
            return _this;
        }
        return ReminderClient;
    }(twAbstractApiClient_1.default));
    TwBookingClient.ReminderClient = ReminderClient;
    var NotificationRuleClient = /** @class */ (function (_super) {
        __extends(NotificationRuleClient, _super);
        function NotificationRuleClient() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.path = 'notificationrules';
            _this.Class = notification_rule_class_1.CNotificationRule;
            return _this;
        }
        NotificationRuleClient.prototype.test = function (id) {
            return __awaiter(this, void 0, void 0, function () {
                var options, res, result, e_2;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            _a.trys.push([0, 2, , 3]);
                            options = this.getOptions();
                            options.url += id + '/test';
                            options.method = 'get';
                            return [4 /*yield*/, (0, axios_1.default)(options)];
                        case 1:
                            res = _a.sent();
                            result = booking_class_1.CBooking.deserialize(res.data);
                            return [2 /*return*/, result];
                        case 2:
                            e_2 = _a.sent();
                            throw e_2;
                        case 3: return [2 /*return*/];
                    }
                });
            });
        };
        return NotificationRuleClient;
    }(twAbstractApiClient_1.default));
    TwBookingClient.NotificationRuleClient = NotificationRuleClient;
})(TwBookingClient = exports.TwBookingClient || (exports.TwBookingClient = {}));
//# sourceMappingURL=twBooking.client.js.map