import TwAbstractApiClient from './twAbstractApiClient';
import TwMinApiClient from './twMinApiClient';
import { CBlogAccount } from '../classes/twblog/blog.account.class';
import { CBlogEntry } from '../classes/twblog/blog.entry.class';
import { CBlogAttachment } from '../classes/twblog/blog.attachment.class';
export declare namespace TwBlogClient {
    namespace Private {
        class BlogAccountClient<T = CBlogAccount> extends TwAbstractApiClient<T> {
            protected readonly path = "accounts";
            Class: typeof CBlogAccount;
        }
        class BlogEntryClient<T = CBlogEntry> extends TwAbstractApiClient<T> {
            protected readonly path = "entries";
            Class: typeof CBlogEntry;
        }
        class BlogAttachmentClient<T = CBlogAttachment> extends TwAbstractApiClient<T> {
            protected readonly path = "attachments";
            Class: typeof CBlogAttachment;
            addAttachment(attachment: CBlogAttachment, file: any): Promise<CBlogAttachment>;
        }
    }
    namespace User {
        class BlogAccountClient<T = CBlogAccount> extends TwMinApiClient<T> {
            protected readonly path = "public/accounts";
            Class: typeof CBlogAccount;
            get(hash: string): Promise<T>;
            getEntries(hash: string): Promise<CBlogEntry[]>;
            addEntry(hash: string, entry: CBlogEntry): Promise<CBlogEntry>;
            updateEntry(hash: string, eid: string, entry: CBlogEntry): Promise<CBlogEntry>;
            deleteEntry(hash: string, eid: string): Promise<CBlogEntry>;
            getEntry(hash: string, eid: string): Promise<CBlogEntry>;
            setUserName(hash: string, user_name: string): Promise<boolean>;
            addAttachment(hash: string, attachment: CBlogAttachment, file: any): Promise<CBlogAttachment>;
            getAttachments(hash: string): Promise<CBlogAttachment[]>;
            getBookings(hash: string): Promise<CBlogEntry.CBlogBooking[]>;
        }
    }
    namespace Public {
        class BlogEntryClient<T = CBlogEntry> extends TwMinApiClient<T> {
            protected readonly path = "public/blogs";
            getEntries(user_name: string): Promise<CBlogEntry[]>;
            getEntry(user_name: string, eid: string): Promise<CBlogEntry>;
        }
    }
}
//# sourceMappingURL=twBlog.client.d.ts.map