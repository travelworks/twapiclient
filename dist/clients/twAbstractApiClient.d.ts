import TwMinApiClient from './twMinApiClient';
export interface ITwListResult<T> {
    items: T[];
    totalCount: number;
}
export interface ITwAbstractApiClient<T> {
    list(query: any | null, sorting: any | null, paging: any | null, projection: any | null): Promise<ITwListResult<T>>;
    get(id: string): Promise<T>;
    post(data: any): Promise<T>;
    patch(data: any, id?: string): Promise<T>;
    put(data: any, id?: string): Promise<T>;
    delete(id: string): Promise<T>;
}
export default class TwAbstractApiClient<T> extends TwMinApiClient<T> implements ITwAbstractApiClient<T> {
    list(query?: any | null, sorting?: any | null, paging?: any | null, projection?: any | null, path?: string | null): Promise<ITwListResult<T>>;
    get(id: string, path?: string | null): Promise<T>;
    post(data: any, path?: string | null): Promise<T>;
    patch(data: any, id?: string, path?: string | null): Promise<T>;
    put(data: any, id?: string, path?: string | null): Promise<T>;
    delete(id: string, path?: string | null): Promise<T>;
}
//# sourceMappingURL=twAbstractApiClient.d.ts.map