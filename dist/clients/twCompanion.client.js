"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwCompanionClient = void 0;
var companion_class_1 = require("../classes/twcompanion/companion.class");
var group_class_1 = require("../classes/twcompanion/group.class");
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var TwCompanionClient;
(function (TwCompanionClient) {
    var Private;
    (function (Private) {
        var CompanionClient = /** @class */ (function (_super) {
            __extends(CompanionClient, _super);
            function CompanionClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'companions';
                _this.Class = companion_class_1.CCompanion;
                return _this;
            }
            return CompanionClient;
        }(twAbstractApiClient_1.default));
        Private.CompanionClient = CompanionClient;
        var GroupClient = /** @class */ (function (_super) {
            __extends(GroupClient, _super);
            function GroupClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'groups';
                _this.Class = group_class_1.CGroup;
                return _this;
            }
            return GroupClient;
        }(twAbstractApiClient_1.default));
        Private.GroupClient = GroupClient;
    })(Private = TwCompanionClient.Private || (TwCompanionClient.Private = {}));
})(TwCompanionClient = exports.TwCompanionClient || (exports.TwCompanionClient = {}));
//# sourceMappingURL=twCompanion.client.js.map