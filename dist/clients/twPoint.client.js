"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwPointClient = void 0;
var axios_1 = __importDefault(require("axios"));
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var point_account_class_1 = require("../classes/twpoint/point.account.class");
var point_activity_class_1 = require("../classes/twpoint/point.activity.class");
var twMinApiClient_1 = __importDefault(require("./twMinApiClient"));
var TwPointClient;
(function (TwPointClient) {
    var Private;
    (function (Private) {
        var PointAccountClient = /** @class */ (function (_super) {
            __extends(PointAccountClient, _super);
            function PointAccountClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'accounts';
                _this.Class = point_account_class_1.CPointAccount;
                return _this;
            }
            PointAccountClient.prototype.postEntry = function (entry, customerID) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += customerID + '/entries';
                    options.method = 'POST';
                    options.data = entry;
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(res.data);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            PointAccountClient.prototype.deleteEntry = function (entryID, customerID) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += customerID + '/entries/' + entryID;
                    options.method = 'DELETE';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(res.data);
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            return PointAccountClient;
        }(twAbstractApiClient_1.default));
        Private.PointAccountClient = PointAccountClient;
        var PointActivityClient = /** @class */ (function (_super) {
            __extends(PointActivityClient, _super);
            function PointActivityClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'activities';
                _this.Class = point_activity_class_1.CPointActivity;
                return _this;
            }
            return PointActivityClient;
        }(twAbstractApiClient_1.default));
        Private.PointActivityClient = PointActivityClient;
    })(Private = TwPointClient.Private || (TwPointClient.Private = {}));
    var Public;
    (function (Public) {
        var PointAccountClient = /** @class */ (function (_super) {
            __extends(PointAccountClient, _super);
            function PointAccountClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'public/accounts';
                _this.Class = point_account_class_1.CPointAccount;
                return _this;
            }
            PointAccountClient.prototype.get = function (hash) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            PointAccountClient.prototype.postStatus = function (status, hash) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += hash + '/status';
                    options.method = 'POST';
                    options.data = { status: status };
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            return PointAccountClient;
        }(twMinApiClient_1.default));
        Public.PointAccountClient = PointAccountClient;
    })(Public = TwPointClient.Public || (TwPointClient.Public = {}));
})(TwPointClient = exports.TwPointClient || (exports.TwPointClient = {}));
//# sourceMappingURL=twPoint.client.js.map