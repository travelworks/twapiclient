"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwBlogClient = void 0;
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var axios_1 = __importDefault(require("axios"));
var twMinApiClient_1 = __importDefault(require("./twMinApiClient"));
var blog_account_class_1 = require("../classes/twblog/blog.account.class");
var blog_entry_class_1 = require("../classes/twblog/blog.entry.class");
var blog_attachment_class_1 = require("../classes/twblog/blog.attachment.class");
var TwBlogClient;
(function (TwBlogClient) {
    var Private;
    (function (Private) {
        var BlogAccountClient = /** @class */ (function (_super) {
            __extends(BlogAccountClient, _super);
            function BlogAccountClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'accounts';
                _this.Class = blog_account_class_1.CBlogAccount;
                return _this;
            }
            return BlogAccountClient;
        }(twAbstractApiClient_1.default));
        Private.BlogAccountClient = BlogAccountClient;
        var BlogEntryClient = /** @class */ (function (_super) {
            __extends(BlogEntryClient, _super);
            function BlogEntryClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'entries';
                _this.Class = blog_entry_class_1.CBlogEntry;
                return _this;
            }
            return BlogEntryClient;
        }(twAbstractApiClient_1.default));
        Private.BlogEntryClient = BlogEntryClient;
        var BlogAttachmentClient = /** @class */ (function (_super) {
            __extends(BlogAttachmentClient, _super);
            function BlogAttachmentClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'attachments';
                _this.Class = blog_attachment_class_1.CBlogAttachment;
                return _this;
            }
            BlogAttachmentClient.prototype.addAttachment = function (attachment, file) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, url, x_amz_acl, x_amz_tagging, putResult, e_1;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 4, , 5]);
                                options = this.getOptions();
                                options.method = 'post';
                                options.data = attachment;
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_attachment_class_1.CBlogAttachment().deserialize(res.data);
                                if (!(result && result.put_url)) return [3 /*break*/, 3];
                                url = new URL(result.put_url);
                                x_amz_acl = url.searchParams.get('x-amz-acl');
                                x_amz_tagging = url.searchParams.get('x-amz-tagging');
                                return [4 /*yield*/, axios_1.default.put(result.put_url, file, {
                                        headers: {
                                            'x-amz-acl': x_amz_acl,
                                            'x-amz-tagging': x_amz_tagging,
                                            'Content-Type': attachment.meta.type
                                        }
                                    })];
                            case 2:
                                putResult = _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/, result];
                            case 4:
                                e_1 = _a.sent();
                                throw e_1;
                            case 5: return [2 /*return*/];
                        }
                    });
                });
            };
            return BlogAttachmentClient;
        }(twAbstractApiClient_1.default));
        Private.BlogAttachmentClient = BlogAttachmentClient;
    })(Private = TwBlogClient.Private || (TwBlogClient.Private = {}));
    var User;
    (function (User) {
        var CBlogBooking = blog_entry_class_1.CBlogEntry.CBlogBooking;
        var BlogAccountClient = /** @class */ (function (_super) {
            __extends(BlogAccountClient, _super);
            function BlogAccountClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'public/accounts';
                _this.Class = blog_account_class_1.CBlogAccount;
                return _this;
            }
            BlogAccountClient.prototype.get = function (hash) {
                var _this = this;
                return new Promise(function (resolve, reject) {
                    var options = _this.getOptions();
                    options.url += hash;
                    options.method = 'get';
                    (0, axios_1.default)(options).then(function (res) {
                        resolve(_this.prepareResult(res));
                    }).catch(function (err) {
                        reject(err);
                    });
                });
            };
            BlogAccountClient.prototype.getEntries = function (hash) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_2;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/entries';
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = blog_entry_class_1.CBlogEntry.deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_2 = _a.sent();
                                throw e_2;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.addEntry = function (hash, entry) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_3;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/entries';
                                options.method = 'post';
                                options.data = entry;
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_entry_class_1.CBlogEntry().deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_3 = _a.sent();
                                throw e_3;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.updateEntry = function (hash, eid, entry) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_4;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/entries/' + eid;
                                options.method = 'put';
                                options.data = entry;
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_entry_class_1.CBlogEntry().deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_4 = _a.sent();
                                throw e_4;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.deleteEntry = function (hash, eid) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_5;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/entries/' + eid;
                                options.method = 'delete';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_entry_class_1.CBlogEntry().deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_5 = _a.sent();
                                throw e_5;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.getEntry = function (hash, eid) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_6;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/entries/' + eid;
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_entry_class_1.CBlogEntry().deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_6 = _a.sent();
                                throw e_6;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.setUserName = function (hash, user_name) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, e_7;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash;
                                options.method = 'put';
                                options.data = { user_name: user_name };
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                _a.sent();
                                return [2 /*return*/, true];
                            case 2:
                                e_7 = _a.sent();
                                throw e_7;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.addAttachment = function (hash, attachment, file) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, url, x_amz_acl, x_amz_tagging, axiosInstance, putResult, e_8;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 4, , 5]);
                                options = this.getOptions();
                                options.url += hash + '/attachments';
                                options.method = 'post';
                                options.data = attachment;
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_attachment_class_1.CBlogAttachment().deserialize(res.data);
                                if (!(result && result.put_url)) return [3 /*break*/, 3];
                                url = new URL(result.put_url);
                                x_amz_acl = url.searchParams.get('x-amz-acl');
                                x_amz_tagging = url.searchParams.get('x-amz-tagging');
                                axiosInstance = axios_1.default.create();
                                return [4 /*yield*/, axiosInstance.put(result.put_url, file, {
                                        headers: {
                                            'x-amz-acl': x_amz_acl,
                                            'x-amz-tagging': x_amz_tagging,
                                            'Content-Type': attachment.meta.type
                                        }
                                    })];
                            case 2:
                                putResult = _a.sent();
                                _a.label = 3;
                            case 3: return [2 /*return*/, result];
                            case 4:
                                e_8 = _a.sent();
                                throw e_8;
                            case 5: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.getAttachments = function (hash) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_9;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/attachments';
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = blog_attachment_class_1.CBlogAttachment.deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_9 = _a.sent();
                                throw e_9;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogAccountClient.prototype.getBookings = function (hash) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, _i, _a, b, object, e_10;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _b.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += hash + '/bookings';
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _b.sent();
                                result = [];
                                for (_i = 0, _a = res.data; _i < _a.length; _i++) {
                                    b = _a[_i];
                                    object = new CBlogBooking();
                                    result.push(object.deserialize(b));
                                }
                                return [2 /*return*/, result];
                            case 2:
                                e_10 = _b.sent();
                                throw e_10;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            return BlogAccountClient;
        }(twMinApiClient_1.default));
        User.BlogAccountClient = BlogAccountClient;
    })(User = TwBlogClient.User || (TwBlogClient.User = {}));
    var Public;
    (function (Public) {
        var BlogEntryClient = /** @class */ (function (_super) {
            __extends(BlogEntryClient, _super);
            function BlogEntryClient() {
                var _this = _super !== null && _super.apply(this, arguments) || this;
                _this.path = 'public/blogs';
                return _this;
            }
            BlogEntryClient.prototype.getEntries = function (user_name) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_11;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += user_name + '/entries';
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = blog_entry_class_1.CBlogEntry.deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_11 = _a.sent();
                                throw e_11;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            BlogEntryClient.prototype.getEntry = function (user_name, eid) {
                return __awaiter(this, void 0, void 0, function () {
                    var options, res, result, e_12;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                _a.trys.push([0, 2, , 3]);
                                options = this.getOptions();
                                options.url += user_name + '/entries/' + eid;
                                options.method = 'get';
                                return [4 /*yield*/, (0, axios_1.default)(options)];
                            case 1:
                                res = _a.sent();
                                result = new blog_entry_class_1.CBlogEntry().deserialize(res.data);
                                return [2 /*return*/, result];
                            case 2:
                                e_12 = _a.sent();
                                throw e_12;
                            case 3: return [2 /*return*/];
                        }
                    });
                });
            };
            return BlogEntryClient;
        }(twMinApiClient_1.default));
        Public.BlogEntryClient = BlogEntryClient;
    })(Public = TwBlogClient.Public || (TwBlogClient.Public = {}));
})(TwBlogClient = exports.TwBlogClient || (exports.TwBlogClient = {}));
//# sourceMappingURL=twBlog.client.js.map