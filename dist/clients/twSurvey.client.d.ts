import TwAbstractApiClient from './twAbstractApiClient';
import { CSurvey } from '../classes/twsurvey/survey.class';
import { CRequest } from '../classes/twsurvey/request.class';
import { CQuestion } from '../classes/twsurvey/question.class';
import TwMinApiClient from './twMinApiClient';
export declare namespace TwSurveyClient {
    namespace Private {
        class SurveyClient<T = CSurvey> extends TwAbstractApiClient<T> {
            protected readonly path = "surveys";
            Class: typeof CSurvey;
        }
        class RequestClient<T = CRequest> extends TwAbstractApiClient<T> {
            protected readonly path = "requests";
            Class: typeof CRequest;
        }
        class QuestionClient<T = CQuestion> extends TwAbstractApiClient<T> {
            protected readonly path = "questions";
            Class: typeof CQuestion;
        }
    }
    namespace Public {
        class SurveyClient<T = CSurvey> extends TwMinApiClient<T> {
            protected readonly path = "public/surveys";
            Class: typeof CSurvey;
            get(id: string): Promise<T>;
        }
        class RequestClient<T = CRequest> extends TwMinApiClient<T> {
            protected readonly path = "public/requests";
            Class: typeof CRequest;
            get(hash: string): Promise<T>;
            put(data: any, id?: string): Promise<T>;
        }
    }
}
//# sourceMappingURL=twSurvey.client.d.ts.map