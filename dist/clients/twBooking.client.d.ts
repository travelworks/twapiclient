import { CCustomer } from '../classes/twbooking/customer.class';
import { CBooking } from '../classes/twbooking/booking.class';
import TwAbstractApiClient, { ITwListResult } from './twAbstractApiClient';
import { CNotificationRule } from '../classes/twbooking/notification.rule.class';
import { CReminder } from '../classes/twbooking/reminder.class';
import { CNotificationBlock } from '../classes/twbooking/notification.block.class';
import { CNotification } from '../classes/twbooking/notification.class';
import TwMinApiClient from './twMinApiClient';
import { INotificationBlock } from '..';
export declare namespace TwBookingClient {
    class CustomerClient<T = CCustomer> extends TwAbstractApiClient<T> {
        protected readonly path = "customers";
        Class: typeof CCustomer;
        getBookings(query?: any, sorting?: any, paging?: any): Promise<ITwListResult<any>>;
    }
    namespace CustomerClient {
        class AbstractCustomerClient<T> extends TwAbstractApiClient<T> {
            protected readonly path = "customers";
            protected readonly subPath: string;
            list(customer_id: string, query?: any, sorting?: any, paging?: any, projection?: any): Promise<ITwListResult<T>>;
            get(customer_id: string, id: string): Promise<T>;
            post(customer_id: string, data: any): Promise<T>;
            patch(customer_id: string, data: any, id?: string): Promise<T>;
            put(customer_id: string, data: any, id?: string): Promise<T>;
            delete(customer_id: string, id: string): Promise<T>;
        }
        export class NotificationClient<T = CNotification> extends AbstractCustomerClient<T> {
            protected readonly subPath = "notifications";
            Class: typeof CNotification;
        }
        export class NotificationBlockClient<T = CNotificationBlock> extends TwMinApiClient<T> {
            Class: typeof CNotificationBlock;
            static getBlockedStatus(block: INotificationBlock, scope: NotificationBlockClient.Scopes, notification?: string): boolean;
            get(cid: string): Promise<CNotificationBlock>;
            patch(cid: string, data: any): Promise<CNotificationBlock>;
            delete(cid: string): Promise<CNotificationBlock>;
            isBlocked(cid: string, scope: NotificationBlockClient.Scopes, notification?: string): Promise<boolean>;
        }
        export namespace NotificationBlockClient {
            enum Scopes {
                survey = "survey",
                companion = "companion",
                point = "point",
                payment = "payment",
                notification = "notification"
            }
        }
        export class NoteClient<T = CCustomer.CNote> extends AbstractCustomerClient<T> {
            protected readonly subPath = "notes";
            Class: typeof CCustomer.CNote;
        }
        export class ReminderClient<T = CReminder> extends AbstractCustomerClient<T> {
            protected readonly subPath = "reminders";
            Class: typeof CReminder;
        }
        export {};
    }
    class ReminderClient<T = CReminder> extends TwAbstractApiClient<T> {
        protected readonly path = "reminders";
        Class: typeof CReminder;
    }
    class NotificationRuleClient<T = CNotificationRule> extends TwAbstractApiClient<T> {
        protected readonly path = "notificationrules";
        Class: typeof CNotificationRule;
        test(id: string): Promise<CBooking[]>;
    }
}
//# sourceMappingURL=twBooking.client.d.ts.map