import { CCompanion } from '../classes/twcompanion/companion.class';
import { CGroup } from '../classes/twcompanion/group.class';
import TwAbstractApiClient from './twAbstractApiClient';
export declare namespace TwCompanionClient {
    namespace Private {
        class CompanionClient<T = CCompanion> extends TwAbstractApiClient<T> {
            protected readonly path = "companions";
            Class: typeof CCompanion;
        }
        class GroupClient<T = CGroup> extends TwAbstractApiClient<T> {
            protected readonly path = "groups";
            Class: typeof CGroup;
        }
    }
}
//# sourceMappingURL=twCompanion.client.d.ts.map