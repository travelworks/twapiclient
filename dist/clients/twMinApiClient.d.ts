import { AxiosRequestConfig, AxiosResponse } from 'axios';
import CAbstractClass from '../classes/abstract.class';
export default abstract class TwMinApiClient<T> {
    private uri;
    protected path: string;
    private key_id;
    private key_secret;
    protected tokenFunction: any | null;
    protected Class: typeof CAbstractClass;
    constructor(uri: string, key_id?: string | null, key_secret?: string | null, tokenFunction?: any | null);
    private addInterceptor;
    protected getOptions(path?: string | null): AxiosRequestConfig;
    protected getUir(): string;
    protected prepareResult(res: AxiosResponse): T | any;
}
//# sourceMappingURL=twMinApiClient.d.ts.map