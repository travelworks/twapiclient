"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TwEmailClient = void 0;
var twAbstractApiClient_1 = __importDefault(require("./twAbstractApiClient"));
var emailTemplate_class_1 = require("../classes/twemail/emailTemplate.class");
var email_class_1 = require("../classes/twemail/email.class");
var TwEmailClient;
(function (TwEmailClient) {
    var EmailClient = /** @class */ (function (_super) {
        __extends(EmailClient, _super);
        function EmailClient() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.path = 'emails';
            _this.Class = email_class_1.CEmail;
            return _this;
        }
        return EmailClient;
    }(twAbstractApiClient_1.default));
    TwEmailClient.EmailClient = EmailClient;
    var EmailTemplateClient = /** @class */ (function (_super) {
        __extends(EmailTemplateClient, _super);
        function EmailTemplateClient() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.path = 'emailtemplates';
            _this.Class = emailTemplate_class_1.CEmailTemplate;
            return _this;
        }
        return EmailTemplateClient;
    }(twAbstractApiClient_1.default));
    TwEmailClient.EmailTemplateClient = EmailTemplateClient;
})(TwEmailClient = exports.TwEmailClient || (exports.TwEmailClient = {}));
//# sourceMappingURL=twEmail.client.js.map